#!/usr/bin/env python

import argparse
import datetime
import json
import nanomsg
import os
import sqlite3
import signal 
import sys
import threading
import time

class Database(object):
    def __init__(self, name = 'default.db'):
        self.name = name
        self.con = None
        self.cur = None

    def connect(self):
        self.con = sqlite3.connect(self.name) 
        self.con.row_factory = sqlite3.Row
        self.cur = self.con.cursor() 
        self.cur.execute('PRAGMA foreign_keys = ON')
        
    def close(self):
        assert self.cur != None
        assert self.con != None

        self.cur.close()
        self.con.close()

    def run_script(self, sql_script):
        assert(self.cur != None)

        self.cur.executescript(sql_script)
        self.con.commit()

def to_timestring(dt):
    # strftime is broken for dates before 1900
    if dt is None: return u''
    dt.replace(microsecond=0, tzinfo=None)
    return unicode(datetime.datetime.strftime(dt, '%Y-%m-%d %H:%M:%S'))

def from_timestring(st):
    import datetime
   # st.replace(microsecond=0, tzinfo=None)
    return datetime.datetime.strptime(st, '%Y-%m-%d %H:%M:%S')

def make_sess(sess_id = None,\
              created = None,\
              updated = None):
    if created is None:
        created = datetime.datetime.now() 
        updated = created

    return {'sess_id':sess_id,\
            'created':created,\
            'updated':updated}

def insert_sess(db, sess):
    assert sess.has_key('created')
    assert sess.has_key('updated')
    assert sess['created'] == sess['updated']
    q = 'INSERT INTO sess(created, updated)\
                VALUES (?, ?)'
    db.cur.execute(q, (to_timestring(sess['created']),
                         to_timestring(sess['updated'])))
    db.con.commit()
    sess['sess_id'] = db.cur.lastrowid
    return sess['sess_id']

def select_all_sess(db):
    q = 'SELECT id, created, updated FROM sess'
    db.cur.execute(q)
    return db.cur.fetchall()

def select_sess(db, sess):
    assert sess.has_key('sess_id')
    q = 'SELECT id, created, updated FROM sess WHERE id = ?'
    db.cur.execute(q, (sess['sess_id'],))
    return db.cur.fetchone()

def update_sess(db, sess):
    assert sess.has_key('sess_id')
    assert sess.has_key('updated')
    q = 'UPDATE sess SET updated = ? WHERE id = ?'
    db.cur.execute(q, (to_timestring(sess['updated']),
                        sess['sess_id']))
    db.con.commit()

def delete_sess(db, sess):
    assert sess.has_key('sess_id')
    q = 'DELETE FROM sess WHERE sess_id = ?'
    db.cur.execute(q, sess['sess_id'])
    db.con.commit()

def purge_sess(db, before):
    q = 'SELECT id FROM sess WHERE datetime(updated) < datetime(?)'
    db.cur.execute(q, (before,))
    rs = db.cur.fetchall() 

    q = 'DELETE FROM sess WHERE datetime(updated) < datetime(?)'
    db.cur.execute(q, (before,))
    db.con.commit()
    return rs

def count_sess(db):
    q = 'SELECT COUNT(*) FROM sess'
    db.cur.execute(q)
    return db.cur.fetchone()

def make_page(page_id = None,\
              site = '',\
              url = '',\
              title = '',\
              date_accessed = '',\
              content_hash = ''):
    return {'page_id':page_id,\
            'site':site,\
            'url':url,\
            'title':title,\
            'date_accessed':date_accessed,\
            'content_hash':content_hash}

def page_from_row(r):
    if r is None: return None
    return make_page(page_id = r[0],
                     site = r[1],
                     url = r[2],
                     title = r[3],
                     date_accessed = r[4],
                     content_hash = r[5])

def pages_from_rows(rs): return map(page_from_row, rs)

def sess_from_row(r):
    return make_sess(sess_id = r[0],
                     created = r[1],
                     updated = r[2])

def sesss_from_rows(rs): return map(sess_from_row, rs)

def insert_page(db, page):
    q = 'INSERT INTO page(site, url, title, date_accessed, content_hash)\
                    VALUES (?, ?, ?, ?, ?)'
    db.cur.execute(q, (page['site'], page['url'],\
                         page['title'],\
                         page['date_accessed'],\
                         page['content_hash']))
    db.con.commit()
    page['page_id'] = db.cur.lastrowid
    return page['page_id']

def delete_page(db, page_id):
    q = 'DELETE FROM page WHERE id = ?'
    db.cur.execute(q, (page_id,))
    db.con.commit()

def update_date_accessed_and_content_hash(db, page):
    q = 'UPDATE page SET date_accessed = ?, content_hash = ? WHERE id = ?'
    db.cur.execute(q, (page['date_accessed'],
                         page['content_hash'],
                         page['page_id']))
    db.con.commit()

def update_content_hash(db, page):
    q = 'UPDATE page SET content_hash = ? WHERE id = ?'
    db.cur.execute(q, (page['content_hash'], page['page_id']))
    db.con.commit()

def update_date_accessed(db, page):
    q = 'UPDATE page SET date_accessed = ? WHERE id = ?'
    db.cur.execute(q, (page['date_accessed'], page['page_id']))
    db.con.commit()

def select_pages_by_site(db, site):
    q = 'SELECT id, site, url, title, date_accessed, content_hash FROM\
            page WHERE site = ?'
    db.cur.execute(q, (site,))
    return db.cur.fetchall()

def select_page_by_url(db, page):
    q = 'SELECT id, site, url, title, date_accessed, content_hash FROM\
                page WHERE url = ?'
    db.cur.execute(q, (page['url'],))
    return db.cur.fetchall()

def select_page_by_id(db, page):
    q = 'SELECT id, site, url, title, date_accessed, content_hash FROM page WHERE id = ?'
    db.cur.execute(q, (page['page_id'],))
    return db.cur.fetchone()

def select_pages_for_site(db, site):
    q = 'SELECT id, site, url, title, date_accessed, content_hash FROM\
                page WHERE site = ?'
    db.cur.execute(q, (site,))
    return db.cur.fetchall()

def is_valid_url(url):
    import re
    regex = re.compile(
        r'^https?://'  # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    return url is not None and regex.search(url)

def is_valid_date(d):
    if d is None: return False
    try:
        nd = from_timestring(d)
        return True
    except ValueError:
        return False

sess_tbl = {}
sess_tbl_lock = threading.Lock()
def sess_get(db, sess_id):
    if sess_tbl.has_key(sess_id):
        sess = make_sess()
        sess['sess_id'] = sess_id
        sess_row = select_sess(db, sess)
        if sess_row is not None:
            sess['created'] = sess_row[1]
            sess['updated'] = from_timestring(sess_row[2])
            update_sess(db, sess)
            return sess
    return None

def sess_populate(db):
    sess_tbl_lock.acquire()
    rs = select_all_sess(db)
    for r in rs:
        sess = make_sess(r[0], r[1], r[2])
        sess_tbl[sess['sess_id']] = sess
    sess_tbl_lock.release()

def sess_purge(db, before):
    sess_tbl_lock.acquire()
    rs = purge_sess(db, to_timestring(before))
    for r in rs:
        sess_id = r[0]
        if sess_tbl.has_key(sess_id):
            del sess_tbl[sess_id]
    sess_tbl_lock.release()

def sess_extend(db, sess_id):
    sess_tbl_lock.acquire()
    sess = make_sess(sess_id=sess_id)
    sess['updated'] = datetime.datetime.now()
    sess_tbl[sess_id] = sess
    update_sess(db, sess)
    sess_tbl_lock.release()
    
def sess_end(db, sess_id):
    sess_tbl_lock.acquire()
    sess = make_sess(sess_id=sess_id)
    delete_sess(db, sess)
    del sess_tbl[sess_id]
    sess_tbl_lock.release()

def sess_start(db):
    sess_tbl_lock.acquire()
    sess = make_sess()
    sess_id = insert_sess(db, sess)
    sess_tbl[sess_id] = sess
    sess_tbl_lock.release()
    return sess

def sess_from_js(db, js):
    if js.has_key('sess_id'):
        sess_id = int(js.get('sess_id'))
        sess = sess_get(db, sess_id)
        if sess is not None:
            return sess
    return None

def run_sess_monitor(args):
    logger = make_logger(args.log_file)
    logger('session monitor started')
    db = Database(args.database)
    db.connect()
    start = datetime.datetime.now()
    half_day = 60 * 60 * 12 
    while True:
        new_now = datetime.datetime.now()
        if (new_now - start) > datetime.timedelta(days=1):
            bf_bct = count_sess(db)
            before = new_now - datetime.timedelta(days=7)
            sess_purge(db, before)
            af_bct = count_sess(db)
            logger('purged sessions at %s, before: %d, after: %d' % (to_timestring(before), bf_bct[0], af_bct[0]))
            start = new_now
        time.sleep(half_day)
        
def run_server(args):
    logger = make_logger(args.log_file)
    db = Database(args.database)
    db.connect()
    skt = nanomsg.Socket(nanomsg.REP)
    skt.bind('tcp://*:%d' % args.port)
    logger('bound %d' % (args.port))
    def cleanup(sig=None, frm=None):
        skt.close()
        sys.exit(0)

    def send_msg(j_m):
        rt = json.dumps(j_m)
        logger('sending rt %s in return' % (rt,))
        skt.send(rt)

    def send_err(msg='error'):
        send_msg({'status':'error', 'msg':msg})

    def send_succ(data={}):
        send_msg({'status':'success', 'data':data})

    def op_with_sess(fn):
        def os(js):
            sess = sess_from_js(db, js)
            if sess is None:
                send_err(msg='session invalid or expired')
                return False
            return fn(js, sess=sess)
        return os

    def op_find(js, sess=None):
        p = make_page()
        p['site'] = js.get('site')
        p['url'] = js.get('url')
        p['page_id'] = js.get('page_id')
        if p['site'] is not None:
            pages_row = select_pages_by_site(db, p['site'])
            send_succ({'pages': map(page_from_row, pages_row) })
        elif p['url'] is not None:
            pages_row = select_page_by_url(db, p)
            send_succ({'pages': map(page_from_row, pages_row)})
        elif p['page_id'] is not None:
            page = page_from_row(select_page_by_id(db, p))
            if page is not None: send_succ({'pages':[page]})
            else: send_succ({'pages':[]})
        return True

    def op_add(js, sess=None):
        site = js.get('site')
        url = js.get('url')
        title = js.get('title')
        date_accessed = js.get('date_accessed')
        content_hash = js.get('content_hash')
        if site is None or\
            url is None or\
            title is None or\
            date_accessed is None or\
            content_hash is None:
            send_err(msg='one or more of (site, url, title, date_accessed, content_hash) was missing')
            return False
        if not len(site) > 0 or\
           not content_hash.isalnum() or\
           not is_valid_url(url) or\
           not is_valid_date(date_accessed):
           send_err(msg='invalid data')
           return False
        pg = make_page(None,\
                       site,\
                       url,\
                       title,\
                       date_accessed,\
                       content_hash)
        pg_id = insert_page(db, pg)
        send_succ({'page_id':pg_id})
        return True

    def op_delete(js, sess=None):
        page_id = js.get('page_id')
        if page_id is None:
            send_err(msg='page_id missing')
        if not type(page_id) == int:
            send_err(msg='page_id not numeric')
        delete_page(db, page_id)
        send_succ()
        return True

    def op_update(js, sess=None):
        p = make_page()
        p['page_id'] = js.get('page_id')
        p['date_accessed'] = js.get('date_accessed')
        p['content_hash'] = js.get('content_hash')
        def send_pages_page_id_succ(pg):
            send_succ({'pages':[{'page_id': pg['page_id']}]})
        if p['page_id'] is None:
            send_err(msg='page_id missing')
        if p['date_accessed'] is not None\
                and p['content_hash'] is not None:
            update_date_accessed_and_content_hash\
                (db, p)
            send_succ({'pages':[{'page_id': p['page_id'],
                                 'content_hash':p['content_hash'],
                                 'date_accessed':p['date_accessed']}]})
        elif p['date_accessed'] is not None and p['content_hash'] is None:
            update_date_accessed(db, p)
            send_succ({'pages':[{'page_id': p['page_id'],
                                 'date_accessed':p['date_accessed'] }]})
        elif p['content_hash'] is not None and p['date_accessed'] is None:
            update_content_hash(db, p)
            send_succ({'pages':[{'page_id':p['page_id'],
                                'content_hash':p['content_hash'] }]})
        return True

    def op_register(js):
        sess = sess_start(db)
        send_succ({'sess_id':sess['sess_id']})
        return True
   
    sess_populate(db)

    print('Carlina 0.01')
    op_lookup = { 'find' : op_with_sess(op_find),
                  'add' : op_with_sess(op_add),
                  'delete' : op_with_sess(op_delete),
                  'register' : op_register, 
                  'update' : op_with_sess(op_update)}
    while True:
        rt = ''
        by = skt.recv()
        js = json.loads(by.decode('utf8'))
        logger(js)
        action = js.get('action')
        if action is None or\
                not op_lookup.has_key(action):
            continue
        op_lookup[action](js)
    cleanup()

class MyThread(threading.Thread):
    def __init__(self, target, *args):
        self.target = target
        self.args = args
        threading.Thread.__init__(self)
 
    def run(self):
        self.target(*self.args)

def make_logger(fname):
    def logger(msg):
        f = open(fname, 'a')
        f.write("%s\n" % (msg,))
        f.close()
    return logger
 
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', help='local port for binding', type=int, default=30089)
    parser.add_argument('-d', '--database', help='carlina database', type=str, required=True)
    parser.add_argument('-l', '--log-file', help='file for logging', type=str, required=False)
    args = parser.parse_args()
    port = args.port
    dbname = args.database
    if (args.log_file is None) or (args.log_file == ''):
        args.log_file = '/tmp/carlina-log.txt'
    if (port > 0) and (port < 0xFFFF):
        server_thr = MyThread(run_server, args)
        monitor_thr = MyThread(run_sess_monitor, args)
        thrs = [ server_thr, monitor_thr ] 
        for t in thrs: t.start()
        for t in thrs: t.join()
    else:
        print('* * * ERROR: port not in range') 
        sys.exit(1)

