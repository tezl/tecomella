#!/usr/bin/env python

import argparse
import nanomsg
import json
import datetime 
import random
import string

def send(s, d):
    j = json.dumps(d)
    #print('sending %s' % (j))
    s.send(j) 

def recv(s):
    by = s.recv()
    out = json.loads(by.decode('utf8'))
    return out

def connect(server, port):
    s = nanomsg.Socket(nanomsg.REQ)
    s.connect('tcp://%s:%d' % (server, port))
    return s

def register(args):
    data = {'action':'register'}
    s = connect(args.server, args.port)
    send(s, data)
    out = recv(s)
    assert out is not None
    assert out.get('status') == 'success'
    sess_id = out.get('data').get('sess_id')
    print(sess_id)
    s.close()

def find(args):
    s = connect(args.server, args.port)
    data = {'action':'find',
            'sess_id':args.sess_id}
    if args.by_url is not None:
        data['url'] = args.by_url
    elif args.by_site is not None:
        data['site'] = args.by_site
    elif args.by_page_id is not None:
        data['page_id'] = args.by_page_id
    
    send(s, data)
    out = recv(s)
    assert out.get('data') is not None

    pages = out.get('data').get('pages')
    for p in pages:
        if args.raw:
            print(p)
        else:
            print("%d | %s\t| %s\t| %s\t| %s\t" % ( p.get('page_id'),\
                                                    p.get('site'),\
                                                    p.get('url'),\
                                                    p.get('date_accessed'),\
                                                    p.get('content_hash')))
    s.close()

def add(args):
    s = connect(args.server, args.port)
    data = {'action':'add',
            'sess_id':args.sess_id,
            'site':args.site,
            'url':args.url,
            'title':args.title,
            'date_accessed':args.date_accessed,
            'content_hash':args.content_hash}
    send(s, data)
    out = recv(s)
    assert out.get('status') == 'success'
    s.close()

def remove(args):
    s = connect(args.server, args.port)
    data = {'action':'delete',
            'sess_id':args.sess_id,
            'page_id':args.page_id}
    send(s, data)
    out = recv(s)
    assert out.get('status') == 'success'
    s.close()

def update(args):
    s = connect(args.server, args.port)
    data = {'action':'update',
            'sess_id':args.sess_id,
            'page_id':args.page_id}

    if args.content_hash is not None:
        data['content_hash'] = args.content_hash

    if args.date_accessed is not None:
        data['date_accessed'] = args.date_accessed

    send(s, data)
    out = recv(s)
    assert out.get('status') == 'success'
    s.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', help='port of remote host', type=int, default=30091)
    parser.add_argument('-s', '--server', help='remote host', type=str, required=True)
    subparsers = parser.add_subparsers(help='sub-command help')

    parser_r = subparsers.add_parser('register', help='register help')
    parser_r.set_defaults(which='register')
    
    parser_f = subparsers.add_parser('find', help='find help')
    parser_f.set_defaults(which='find')
    parser_f.add_argument('--sess-id', help='session id', type=str, required=True)
    parser_f.add_argument('-r', '--raw', help='print raw output', action='store_true')
    group = parser_f.add_mutually_exclusive_group(required=True)

    group.add_argument('--by-url', help='by url')
    group.add_argument('--by-site', help='by site')
    group.add_argument('--by-page-id', help='by id')


    parser_a = subparsers.add_parser('add', help='add help')
    parser_a.set_defaults(which='add')
    parser_a.add_argument('--site', help='site', type=str, required=True)
    parser_a.add_argument('--url', help='url', type=str, required=True)
    parser_a.add_argument('--title', help='title', type=str, required=True)
    parser_a.add_argument('--date-accessed', help='date accessed', type=str, required=True)
    parser_a.add_argument('--content-hash', help='content hash', type=str, required=True)
    parser_a.add_argument('--sess-id', help='session id', type=str, required=True)

    parser_d = subparsers.add_parser('remove', help='remove help')
    parser_d.set_defaults(which='remove')
    parser_d.add_argument('--page-id', help='page id', type=int, required=True)
    parser_d.add_argument('--sess-id', help='session id', type=str, required=True)

    parser_u = subparsers.add_parser('update', help='update help')
    parser_u.set_defaults(which='update')
    parser_u.add_argument('--page-id', help='page id', type=int, required=True)
    parser_u.add_argument('--sess-id', help='session id', type=str, required=True)

    parser_u.add_argument('--content-hash', help='content hash', type=str, required=False)
    parser_u.add_argument('--date-accessed', help='date accessed', type=str, required=False)

    args = parser.parse_args()
    if args.which == 'update':
        if not args.content_hash and not args.date_accessed:
            parser.error('update requires at least --content-hash or --date-accessed')
            sys.exit(1)
    
    lkup = { 'register' : register,
             'find' : find,
             'remove' : remove,
             'update' : update,
             'add' : add }
    if lkup.has_key(args.which):
        lkup[args.which](args)
    else:
        print('invalid arguments supplied')
        sys.exit(0)
    
