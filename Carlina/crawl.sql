-- 
-- Izaak 2014
--

BEGIN;
DROP TABLE IF EXISTS page;
CREATE TABLE page (
    id INTEGER PRIMARY KEY NOT NULL,
    site TEXT NOT NULL,
    url TEXT NOT NULL,
    title TEXT NOT NULL,
    date_accessed TEXT NOT NULL,
    content_hash TEXT NULL
);
CREATE INDEX page_url_idx ON page(url);
CREATE INDEX page_accessed_date_idx ON page(date_accessed);
COMMIT;

BEGIN;
DROP TABLE IF EXISTS sess;
CREATE TABLE sess (
    id INTEGER PRIMARY KEY NOT NULL,
    created TEXT NOT NULL,
    updated TEXT NOT NULL
);
CREATE INDEX updated_idx ON sess(updated);
COMMIT;

