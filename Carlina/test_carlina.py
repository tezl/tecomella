import nanomsg
import json
import datetime 
import random
import string

G_now = datetime.datetime.now()
G_now.replace(microsecond=0, tzinfo=None)

def send(s, d):
    j = json.dumps(d)
    #print('sending %s' % (j))
    s.send(j) 

def recv(s):
    by = s.recv()
    out = json.loads(by.decode('utf8'))
    return out

def connect():
    s = nanomsg.Socket(nanomsg.REQ)
    s.connect('tcp://:30089')
    return s

def random_string(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def test_register():
    s = connect()
    data = {'action':'register'}
    send(s, data)
    out = recv(s)
    assert out is not None
    assert out.get('status') == 'success'
    s.close()

def register(s):
    data = {'action':'register'}
    send(s, data)
    out = recv(s)
    assert out is not None
    assert out.get('status') == 'success'
    sess_id = out.get('data').get('sess_id')
    return sess_id

def test_find():
    s = connect()
    sess_id = register(s)
    site = random_string()
    base_url = 'http://www.example.org'
    pgs = [{'name':'first.html', 'page_id':None},\
           {'name':'second.html', 'page_id':None},\
           {'name':'third.html', 'page_id':None}]
    def mk_url(u): return '%s/%s' % (base_url, u)
    def q_add(p):
        data = {'action':'add',\
                'site':site,\
                'url':mk_url(p['name']),\
                'title':'new page',
                'date_accessed':G_now.strftime("%Y-%m-%d %H:%M:%S"),\
                'content_hash':'niks',
                'sess_id':sess_id}
        send(s, data)
        out = recv(s)
        assert out.get('status') == 'success'
        out_data = out.get('data')
        page_id = out_data.get('page_id')
        assert type(page_id) == int
        p['page_id'] = page_id
    def q_find(p):
        data = {'action':'find',
                'page_id':p['page_id'],
                'sess_id':sess_id}
        send(s, data)
        out = recv(s)
        def single_page(o):
            assert o.get('data') is not None
            x = o.get('data').get('pages')
            assert len(x) == 1
            assert x[0]['url'] == mk_url(p['name'])
        single_page(out)

        data = {'action':'find',
                'url':mk_url(p['name']),
                'sess_id':sess_id}
        send(s, data)
        out = recv(s)
        single_page(out)
    for p in pgs:
        q_add(p)
        q_find(p)
    data= {'action':'find',
            'site':site,
            'sess_id':sess_id}
    send(s, data)
    out = recv(s)
    assert out is not None
    assert out.get('status') == 'success'
    opgs = out.get('data').get('pages')
    assert len(opgs) == 3
    urls = [ mk_url(p['name']) for p in pgs ]
    for p in opgs:
        assert p['url'] in urls 
        data = {'action':'delete',
                'page_id':p['page_id'],
                'sess_id':sess_id}
        send(s, data)
        out = recv(s)
        assert out.get('status') == 'success'
    s.close()

def test_add():
    global G_now
    s = connect()
    sess_id = register(s)
    data = {'action':'add',
            'site':'test',
            'url':'http://www.example.org',
            'title':'new page',
            'date_accessed':G_now.strftime("%Y-%m-%d %H:%M:%S"),
            'content_hash':'niks',
            'sess_id':sess_id}
    send(s, data)
    out = recv(s)
    assert out.get('status') == 'success'
    page_id = out.get('data').get('page_id')
    assert page_id is not None and type(page_id) == int 
    data = {'action':'delete',
            'page_id':page_id,
            'sess_id':sess_id}
    send(s, data)
    out = recv(s)
    assert out.get('status') == 'success'
    s.close()

def test_update():
    s = connect()
    sess_id = register(s)
    data = {'action':'add',
            'site':'test',
            'url':'http://www.example.org',
            'title':'new page',
            'date_accessed':G_now.strftime("%Y-%m-%d %H:%M:%S"),
            'content_hash':'niks',
            'sess_id':sess_id}
    send(s, data)
    out = recv(s)
    assert out.get('status') == 'success'
    page_id = out.get('data').get('page_id')
    assert page_id is not None and type(page_id) == int 
    mdate =\
            datetime.datetime(year=2000,month=1,day=2,\
                              hour=1,minute=34,second=45)\
                                .strftime('%Y-%m-%d %H:%M:%S')
    data = {'action':'update',
            'page_id':page_id,
            'content_hash':'niksjongen',
            'date_accessed':mdate,
            'sess_id':sess_id}
    send(s, data)
    out = recv(s)
    assert out.get('status') == 'success'
    data = {'action':'find',
            'page_id':page_id,
            'sess_id':sess_id}
    send(s, data)
    out = recv(s)
    page = out.get('data').get('pages')[0]
    assert page['content_hash'] == 'niksjongen'
    assert page['date_accessed'] == mdate

    data = {'action':'update',
            'page_id':page_id,
            'content_hash':'neejongen',
            'sess_id':sess_id}
    send(s, data)
    out = recv(s)
    assert out.get('status') == 'success'
    data = {'action':'find',
            'page_id':page_id,
            'sess_id':sess_id}
    send(s, data)
    out = recv(s)
    page = out.get('data').get('pages')[0]
    assert page['content_hash'] == 'neejongen'

    mdate =\
            datetime.datetime(year=2001,month=1,day=2,\
                              hour=1,minute=34,second=45)\
                                .strftime('%Y-%m-%d %H:%M:%S')
    data = {'action':'update',
            'page_id':page_id,
            'date_accessed':mdate,
            'sess_id':sess_id}
    send(s, data)
    out = recv(s)
    assert out.get('status') == 'success'
    data = {'action':'find',
            'page_id':page_id,
            'sess_id':sess_id}
    send(s, data)
    page = out.get('data').get('pages')[0]
    assert page['date_accessed'] == mdate
    s.close()

def test_delete():
    s = connect() 
    sess_id = register(s)
    data = {'action':'add',
            'site':'test',
            'url':'http://www.example.org',
            'title':'new delete page',
            'date_accessed':G_now.strftime("%Y-%m-%d %H:%M:%S"),
            'content_hash':'niks',
            'sess_id':sess_id}
    send(s, data)
    out = recv(s)
    assert out.get('status') == 'success'
    out_page = out.get('data')
    assert out_page is not None
    page_id = out_page['page_id']
    assert type(page_id) == int 
    data = {'action':'delete',
            'page_id':page_id,
            'sess_id':sess_id}
    send(s, data)
    out = recv(s)
    assert out.get('status') == 'success'
    data = {'action':'find',
            'page_id':page_id,
            'sess_id':sess_id}
    send(s, data)
    out = recv(s)
    assert out.get('status') == 'success'
    assert len(out.get('data').get('pages')) == 0
    s.close()

if __name__ == '__main__':
    tests = [ ('test_register', test_register),\
              ('test_find', test_find),\
              ('test_add', test_add),\
              ('test_update', test_update),\
              ('test_delete', test_delete) 
               ]
    for t in tests:
        out = u'passed'
        try:
            t[1]()
        except AssertionError:
            out = u'failed'
        print('%s: %s' % (t[0], out))
   

