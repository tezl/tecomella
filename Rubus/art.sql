-- 
-- Izaak 2014
--

BEGIN;
DROP TABLE IF EXISTS artwork;
CREATE TABLE artwork (
    id INTEGER PRIMARY KEY,
    title TEXT NOT NULL,
    date_produced TEXT,
    details TEXT,
    reference TEXT,
    filename TEXT,
    artist_id INTEGER NOT NULL,
    artwork_summary_id INTEGER,

    FOREIGN KEY (artist_id) REFERENCES artist(id),
    FOREIGN KEY (artwork_summary_id) REFERENCES artwork_summary(id) ON DELETE SET NULL
);
CREATE INDEX artwork_title_idx ON artwork(title);
CREATE INDEX artwork_reference_idx ON artwork(reference);
COMMIT;

BEGIN;
DROP TABLE IF EXISTS artist;
CREATE TABLE artist (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    sort_name TEXT,
    start_date TEXT NOT NULL,
    end_date TEXT NOT NULL,

    UNIQUE(name) ON CONFLICT REPLACE 
);
CREATE INDEX artist_name_idx ON artist(name);
CREATE INDEX artist_sort_name_idx ON artist(sort_name);
COMMIT;

BEGIN;
DROP TABLE IF EXISTS artwork_summary;
CREATE TABLE artwork_summary (
    id INTEGER PRIMARY KEY,
    summary TEXT
);
COMMIT;

BEGIN;
DROP TABLE IF EXISTS tracker;
CREATE TABLE tracker (
    id INTEGER PRIMARY KEY,
    artwork_id INTEGER NOT NULL,
    date_accessed TEXT,
    view_count INTEGER NOT NULL,

    FOREIGN KEY (artwork_id) REFERENCES artwork(id)
);
COMMIT;

