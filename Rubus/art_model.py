import model

def make_artwork(artwork_id = None,\
                 title = '',\
                 date_produced = '',\
                 details = '',\
                 reference = '',\
                 filename = '',\
                 artist_id = None,\
                 artwork_summary_id = None): 
    
    return {'artwork_id':artwork_id,\
            'title':title,\
            'date_produced':date_produced,\
            'details':details,\
            'reference':reference,\
            'artist_id':artist_id,\
            'artwork_summary_id':artwork_summary_id,
            'filename':filename}

def make_artist(artist_id = None,\
                name = '',\
                sort_name = '',\
                start_date = '',\
                end_date = ''):

    return {'artist_id':artist_id,\
            'name':name,\
            'sort_name':sort_name,\
            'start_date':start_date,\
            'end_date':end_date}

def make_artwork_summary(artwork_summary_id = None,\
                         summary = ''):

    return {'artwork_summary_id':artwork_summary_id,\
            'summary':summary}

class ArtDatabase(model.Database):
    
    def __init__(self, name = 'art.db'):
        super(ArtDatabase, self).__init__(name)

    def insert_artwork(self, artwork, artwork_summary, artist):
        assert self.cur is not None
        assert type(artwork) is dict
        assert type(artwork_summary) is dict
        assert type(artist) is dict 
        q = 'SELECT id FROM artwork WHERE title = ?'
        ex = self.cur.execute(q, (artwork['title'],))
        r = ex.fetchone()
        if r is not None:
            artwork['artwork_id'] = int(r[0])
            return artwork

        artist['artist_id'] = self.insert_artist(artist)

        artwork_summary['artwork_summary_id'] =\
                        self.insert_artwork_summary(\
                        artwork_summary)
        q = 'INSERT INTO artwork(title, date_produced, details,\
                                 reference, filename, artist_id,\
                                 artwork_summary_id)\
                VALUES (?, ?, ?, ?, ?, ?, ?)'
        dd = self.to_timestring(artwork['date_produced'])

        self.cur.execute(q, (artwork['title'],\
                             dd,\
                             artwork['details'],\
                             artwork['reference'],\
                             artwork['filename'],\
                             artist['artist_id'],\
                             artwork_summary['artwork_summary_id']))
        self.con.commit()

    def select_artwork(self, artwork_id):
        assert type(artwork_id) is int
        q = 'SELECT awk.id, awk.title, awk.date_produced,\
                    awk.details, awk.reference, awk.filename,\
                    awk.artist_id,\
                    awk.artwork_summary_id,\
                    ar.id, ar.name, ar.sort_name,\
                    ar.start_date, ar.end_date,\
                    awy.id, awy.summary\
                        FROM\
                            artwork AS awk\
                                INNER JOIN\
                            artist AS ar ON awk.artist_id = ar.id\
                                INNER JOIN\
                            artwork_summary AS awy\
                                ON awk.artwork_summary_id = awy.id\
                    WHERE awk.id = ?\
                    ORDER BY awk.title ASC'
        ex = self.cur.execute(q, (artwork_id,))
        r = ex.fetchone()
        return r

    def select_artwork_by_title(self, artwork_title):
        assert type(artwork_title) is str or type(artwork_title) is unicode
        q = 'SELECT awk.id, awk.title, awk.date_produced,\
                    awk.details, awk.reference, awk.artist_id,\
                    awk.artwork_summary_id,\
                    awk.filename,\
                    ar.id, ar.name, ar.sort_name,\
                    ar.start_date, ar.end_date,\
                    awy.id, awy.summary\
                        FROM\
                            artwork AS awk\
                                INNER JOIN\
                            artist AS ar ON awk.artist_id = ar.id\
                                INNER JOIN\
                            artwork_summary AS awy\
                                ON awk.artwork_summary_id = awy.id\
                    WHERE awk.title = ?'
        ex = self.cur.execute(q, (artwork_title,))
        r = ex.fetchone()
        return r

    def select_artworks_by_artist_name(self, artist_name):
        q = 'SELECT awk.id, awk.title, awk.date_produced,\
                    awk.details, awk.reference, awk.artist_id,\
                    awk.artwork_summary_id,\
                    awk.filename,\
                    ar.id, ar.name, ar.sort_name,\
                    ar.start_date, ar.end_date,\
                    awy.id, awy.summary\
                        FROM\
                            artwork AS awk\
                               INNER JOIN\
                            artist AS ar ON awk.artist_id = ar.id\
                                INNER JOIN\
                            artwork_summary AS awy\
                            ON awk.artwork_summary_id = awy.id\
                        WHERE ar.name = ?\
                        ORDER BY awk.title ASC'
        ex = self.cur.execute(q, (artist_name,))
        rs = ex.fetchall()
        return rs

    def select_artworks_by_reference(self, reference):
        assert type(reference) is str or type(reference) is unicode
        q = 'SELECT awk.id, awk.title, awk.date_produced,\
                    awk.details, awk.reference,\
                    awk.filename,\
                    awk.artist_id,\
                    awk.artwork_summary_id,\
                    ar.id, ar.name, ar.sort_name,\
                    ar.start_date, ar.end_date,\
                    awy.id, awy.summary\
                        FROM\
                            artwork AS awk\
                               INNER JOIN\
                            artist AS ar ON awk.artist_id = ar.id\
                                INNER JOIN\
                            artwork_summary AS awy\
                            ON awk.artwork_summary_id = awy.id\
                        WHERE awk.reference = ?\
                        ORDER BY awk.title ASC'
        ex = self.cur.execute(q, (reference,))
        rs = ex.fetchall()
        return rs

    def delete_artwork(self, artwork_id):
        q = 'DELETE FROM artwork_summary WHERE id\
                    IN (SELECT artwork_summary_id FROM artwork\
                        WHERE artwork.id = ?)'
        self.cur.execute(q, (artwork_id,)) 
        q = 'DELETE FROM artwork WHERE id = ?'
        self.cur.execute(q, (artwork_id,))
        self.con.commit()

    def insert_artist(self, artist):
        q = 'SELECT id FROM artist WHERE name = ?'
        ex = self.cur.execute(q, (artist['name'],))
        r = ex.fetchone()
        if r is not None:
            artist['artist_id'] = int(r[0])
            return artist['artist_id']
        def make_sort_name(x):
            s = x.split(' ')
            l = len(s)
            if l > 1: return s[-1]
            elif l == 1: return s[0]
            else: return x
            return u''
        q = 'INSERT INTO artist(name, sort_name,\
                                start_date, end_date)\
                    VALUES (?, ?, ?, ?)'
        assert artist['artist_id'] == None
        if artist['start_date'] == None: sd = u''
        else: sd = self.to_timestring(artist['start_date']) 
        if artist['end_date'] == None: ed = u''
        else: ed = self.to_timestring(artist['end_date'])
        sname = u''
        if artist['sort_name'] is None or artist['sort_name'] == u'':
            sname = make_sort_name(artist['name'])
        else:
            sname = artist['sort_name']
        self.cur.execute(q, (artist['name'], sname, sd, ed))
        self.con.commit()
        return self.cur.lastrowid

    def delete_artist(self, artist_id):
        q = 'DELETE FROM artwork_summary WHERE id\
                    IN (SELECT artwork_summary_id FROM artwork\
                        WHERE artwork.artist_id = ?)'
        self.cur.execute(q, (artist_id,))
        q = 'DELETE FROM artwork WHERE artist_id = ?'
        self.cur.execute(q, (artist_id,))
        q = 'DELETE FROM artist WHERE id = ?'
        self.cur.execute(q, (artist_id,))
        self.con.commit()

    def select_artist(self, artist_id):
        q = 'SELECT id, name, sort_name,\
                    start_date, end_date FROM artist WHERE id = ?'
        ex = self.cur.execute(q, (artist_id, ))
        rs = ex.fetchone()
        if rs is not None:
            artist = make_artist(rs[0], rs[1], rs[2], rs[3], rs[4])
            return artist
        return None

    def select_artist_by_name(self, artist_name):
        q = 'SELECT id, name, sort_name,\
                    start_date, end_date\
                FROM artist WHERE name LIKE ?'
        ex = self.cur.execute(q, ('%' + artist_name + '%',))
        rs = ex.fetchone()
        if rs is not None:
            artist = make_artist(rs[0], rs[1], rs[2], rs[3], rs[4])
            return artist
        return None

    def select_artists_by_period(self, start_date, end_date):
        q = 'SELECT id,\
                    name,\
                    sort_name,\
                    start_date,\
                    end_date\
                FROM artist\
                    WHERE start_date > datetime(?)\
                      AND end_date <= datetime(?)'
        ex = self.cur.execute(q,\
            (self.to_timestring(start_date),\
             self.to_timestring(end_date)))
        rs = ex.fetchall()
        if rs == None:
            return None
        return [ make_artist(r[0], r[1], r[2], r[3], r[4]) \
                    for r in rs ]

    def insert_artwork_summary(self, artwork_summary):
        assert type(artwork_summary) is dict
        q = 'INSERT INTO artwork_summary(summary) VALUES (?)'
        self.cur.execute(q, (artwork_summary['summary'],))
        self.con.commit()
        lastid =  self.cur.lastrowid
        assert(type(lastid) is int)
        return lastid

    def delete_artwork_summary(self, artwork_summary_id):
        assert type(artwork_summary_id) is int
        q = 'DELETE FROM artwork_summary WHERE id = ?'
        self.cur.execute(q, (artwork_summary_id,))
        self.con.commit()

    def select_artwork_summary(self, artwork_summary_id):
        assert type(artwork_summary_id) is int
        q = 'SELECT id, summary FROM artwork_summary WHERE id = ?'
        ex = self.cur.execute(q, (artwork_summary_id,))
        r = ex.fetchone()
        if r is not None:
            artwork_summary = make_artwork_summary(r[0], r[1])
            return artwork_summary
        return None

