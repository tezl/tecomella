import PyQt4
from PyQt4 import QtGui
from PyQt4 import QtCore
import random

class ArtworkView(QtGui.QGraphicsView):
    def __init__(self, parent=None):
        super(ArtworkView, self).__init__(parent)
        self.text_callback = None
        self.artworks = []
        self.pixmapCache = {}
        self.current_pixmap = None
        self.setFocusPolicy(QtCore.Qt.StrongFocus)

    def loadImageIntoViewer(self, filename):
        sc = QtGui.QGraphicsScene()
        sz  = self.size()
        border = 30 
        if filename in self.pixmapCache:
            self.current_pixmap = self.pixmapCache[filename]
        else:
            self.current_pixmap = QtGui.QPixmap(filename)# .scaled(sz.width() - border,\
                                         #      sz.height() - border,\
                                         #      PyQt4.QtCore.Qt.KeepAspectRatio)
            self.pixmapCache[filename] = self.current_pixmap 
        sc.addPixmap(self.current_pixmap.scaled(sz.width() - border, sz.height() - border, PyQt4.QtCore.Qt.KeepAspectRatio))
        self.setScene(sc)

    def pixmap(self):
       return self.current_pixmap 

    def loadRandomImage(self):
        a = random.choice(self.artworks)
        if self.text_callback != None:
            self.text_callback(a)
        self.setToolTip(a.data['details'])
        self.loadImageIntoViewer(a.data['filename'])

    def mousePressEvent(self, qmouseevent):
        self.loadRandomImage()

    def keyPressEvent(self, qkeypressevent):
        if qkeypressevent.key() == QtCore.Qt.Key_Escape:
            if self.isFullScreen():
                self.close()
