
import sqlite3

class Database(object):
    def __init__(self, name = 'default.db'):
        self.name = name
        self.con = None
        self.cur = None

    def connect(self):
        self.con = sqlite3.connect(self.name) 
        self.con.row_factory = sqlite3.Row
        self.cur = self.con.cursor() 
        self.cur.execute('PRAGMA foreign_keys = ON')
        
    def close(self):
        assert self.cur != None
        assert self.con != None

        self.cur.close()
        self.con.close()

    def run_script(self, sql_script):
        assert(self.cur != None)

        self.cur.executescript(sql_script)
        self.con.commit()

    def to_timestring(self, dt):
        # strftime is broken for dates before 1900
        if dt is None: return u''
        return unicode(dt.isoformat())

    def from_timestring(self, st):
        import datetime
        st.replace(microsecond=0, tzinfo=None)
        return datetime.datetime.strptime('%Y-%m-%d %H:%M:%S', st)

