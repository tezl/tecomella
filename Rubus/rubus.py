import sys
import PyQt4
from PyQt4 import QtGui, uic
from PyQt4 import QtCore
from PyQt4.QtCore import QObject, SIGNAL
from art_model import make_artwork, make_artist, make_artwork_summary
from artworkview import ArtworkView

class Artwork(object):
    def __init__(self):
        self.data = make_artwork() 
        self.artist = None
        self.summary = None

class ArtworkSummary(object):
    def __init__(self):
        self.data = make_artwork_summary() 

class Artist(object):
    def __init__(self):
        self.data = make_artist() 

G_artworks = [ Artwork(),\
               Artwork(),\
               Artwork(),\
               Artwork() ]
G_artists = [ Artist(),
              Artist(),
              Artist(),
              Artist()]
G_artwork_summaries = [ ArtworkSummary(),\
                        ArtworkSummary(),\
                        ArtworkSummary(),\
                        ArtworkSummary() ]

G_artworks[0].artist = G_artists[0] 
G_artworks[1].artist = G_artists[1] 
G_artworks[2].artist = G_artists[2] 
G_artworks[3].artist = G_artists[3] 

G_artworks[0].artwork_summary = G_artwork_summaries[0]
G_artworks[1].artwork_summary = G_artwork_summaries[1]
G_artworks[2].artwork_summary = G_artwork_summaries[2]
G_artworks[3].artwork_summary = G_artwork_summaries[3]

G_artworks[0].data['title'] = 'The Uncertainty of the Poet'
G_artworks[0].data['date_produced'] = '1913'
G_artworks[0].data['details'] = '998x443 oil on cotton'
G_artworks[0].data['filename'] = 'artwork_a.jpg'
G_artworks[0].artwork_summary.data['summary'] =\
"""<p>A wise old owl lived in an oak
The more he saw the less he spoke
The less he spoke the more he heard.
Why can't we all be like that wise old bird?</p>"""
G_artworks[0].artist.data['name'] = 'Giorgio de Chirico'

G_artworks[1].data['title'] = 'The Metamorphosis of Narcissus'
G_artworks[1].data['date_produced'] = '1937'
G_artworks[1].data['details'] = '102x239 metal on wood'
G_artworks[1].data['filename'] = 'artwork_b.jpg'
G_artworks[1].artwork_summary.data['summary'] =\
"""<p>There was a man lived in the moon,
Lived in the moon, lived in the moon
There was a man lived in the moon,
And his name was Aiken Drum.</p>"""
G_artworks[1].artist.data['name'] = 'Salvador Dali'

G_artworks[2].data['title'] = 'Marguerite Kelsey'
G_artworks[2].data['date_produced'] = '1928'
G_artworks[2].data['details'] = '643x212 acrylic on board'
G_artworks[2].data['filename'] = 'artwork_c.jpg'
G_artworks[2].artwork_summary.data['summary'] =\
"""<p>An apple a day keeps the doctor away
Apple in the morning - Doctor's warning
Roast apple at night - starves the doctor outright
Eat an apple going to bed - knock the doctor on the head
Three each day, seven days a week - ruddy apple, ruddy cheek</p>"""
G_artworks[2].artist.data['name'] = 'Meredith Frampton'

G_artworks[3].data['title'] = "The Queen's Dish"
G_artworks[3].data['date_produced'] = '1932'
G_artworks[3].data['details'] = '123x234 oil on canvas'
G_artworks[3].data['filename'] = 'artwork_d.jpg'
G_artworks[3].artwork_summary.data['summary'] =\
"""<p>As I was going to St. Ives I met a man with seven wives,
Each wife had seven sacks, each sack had seven cats,
Each cat had seven kits: kits, cats, sacks and wives,
How many were going to St. Ives?</p>"""
G_artworks[3].artist.data['name'] = 'David Jones'

class RubusTableModel(QtCore.QAbstractTableModel):
    def __init__(self):
        super(RubusTableModel, self).__init__()
        self.table = ( (1, 2, 3), (4, 5, 6), (7, 8, 9) )
        self.size_hint = QtCore.QSize(100, 25)

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.table)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.table[0])

    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                headers = {0:'Apple', 1:'Ball', 2:'Cat'}
                return QtCore.QVariant(headers[section])
            elif orientation == QtCore.Qt.Vertical:
                return section
        elif role == QtCore.Qt.SizeHintRole:
            # TODO: calculate appropriate size of columns based on results
            return self.size_hint
        return QtCore.QVariant.Invalid

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.DisplayRole:
            i = index.row()
            j = index.column()
            return self.table[i][j]
        elif role == QtCore.Qt.SizeHintRole:
            return self.size_hint
        else:
            return QtCore.QVariant()

class RubusSearchDialog(QtGui.QDialog):
    def __init__(self):
        super(RubusSearchDialog, self).__init__()
        uic.loadUi('ui_search_dialog.ui', self)
        self.table_model = RubusTableModel()
        self.searchTableView.setModel(self.table_model)
        self.searchTableView.verticalHeader().hide()
        self.searchTableView.horizontalHeader().setStretchLastSection(True)

    def resizeEvent(self, event):
        w = (self.width() - 50) / 3
        self.searchTableView.setColumnWidth(0, w)
        self.searchTableView.setColumnWidth(1, w)
        self.searchTableView.setColumnWidth(2, w)
        super(RubusSearchDialog, self).resizeEvent(event)


class RubusWindow(QtGui.QMainWindow):
    def __init__(self):
        global G_artworks
        super(RubusWindow, self).__init__()
        uic.loadUi('ui_viewer.ui', self)
        self.setWindowTitle('Rubus Art Viewer')
        self.viewerGraphicsView.artworks = G_artworks
        self.viewerGraphicsView.text_callback = self.setArtworkSummary
        self.viewerGraphicsView.loadRandomImage()
        self.action_Search.triggered.connect(self.on_actionSearch_triggered)
        self.action_Fullscreen.triggered.connect(self.on_actionFullscreen_triggered)
        self.search_dialog = RubusSearchDialog()
        self.fullscreen_viewer = ArtworkView()
        self.show()

    def setArtworkSummary(self, a):
        title_text = '<h3>%s</h3><p><em>(%s &mdash; %s)</em></p>' % (a.data['title'],\
                                                   a.artist.data['name'],\
                                                   a.data['date_produced'])
        self.detailsTextEdit.setText(title_text + a.artwork_summary.data['summary'])

    def on_actionSearch_triggered(self):
        self.search_dialog.show()

    def on_actionFullscreen_triggered(self):
        dw = QtGui.QDesktopWidget()
        sz = dw.availableGeometry(dw.primaryScreen())
        px = self.viewerGraphicsView.pixmap().\
            scaled(sz.width(), sz.height(), QtCore.Qt.KeepAspectRatio)
        sc = QtGui.QGraphicsScene()
        sc.addPixmap(px)
        self.fullscreen_viewer.setScene(sc)
        self.fullscreen_viewer.showFullScreen()
        
if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    window = RubusWindow()
    sys.exit(app.exec_())
