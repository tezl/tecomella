import signal
import sys
import random
import os
import pickle
import requests
import time
from bs4 import BeautifulSoup

class MineState(object):
    def __init__(self):
        self.root = 'http://localhost:8080'
        self.current_url = '/'
        self.next_link = ''
        self.prev_link = ''
        self.result_page_counter = 0
        self.profile_links = []
        self.profile_links_i = 0
        self.rnd = None 

class Page(object):
    def __init__(self):
        self.url = ''
        self.data = ''
        self.soup = None
    def load(self):
        self.data = requests.get(self.url)
        self.soup = BeautifulSoup(self.data.content)
    def next_link(self): return None
    def prev_link (self): return None
    def content_links(self): return []

class TestPage(object):
    def __init_(self):
        self.url = ''
        self.data = ''
        self.soup = None

    def load(self):
        self.data = requests.get(self.url)
        self.soup = BeautifulSoup(self.data.content)

    def next_link(self):
        assert self.soup is not None
        p =self.soup.find(id='next_page')
        if p is not None:
            return p.get('href')
        return None

    def prev_link(self):
        assert self.soup is not None
        p = self.soup.find(id='prev_page')
        if p is not None:
            return p.get('href')
        return None

    def content_links(self):
        assert self.soup is not None
        return [a.get('href') for a in self.soup.find(id='content').find_all('a')]


class Mine(object):
    def __init__(self):
        self.save_state = False
        pass

    def run(self):
        pickle_file = 'pickle_file'
        ms = None
        resuming = False
        if os.path.isfile(pickle_file):
            f = open(pickle_file, 'r+')
            ms = pickle.load(f)
            f.close()

            if ms.profile_links_i + 1 < ms.profile_links_len:
                ms.profile_links_i += 1
                print 'starting at %s %d, result page: %d' %\
                            (ms.profile_links[ms.profile_links_i],\
                             ms.profile_links_i, ms.result_page_counter)
            else:
                print 'complete, starting new result page'
                # load new current url, ready for start!
                pg = Page()
                pg.url = ms.root + ms.current_url
                pg.load()
                ms.current_url = pg.next_link()
                pg_next = Page()
                pg_next.url = ms.root + ms.current_url
                pg_next.load()
                ms.profile_links = pg_next.content_links()
                ms.profile_links_i = 0
                ms.profile_links_len = len(ms.profile_links)
                ms.result_page_counter += 1
                ms.next_link = pg_next.next_link()
                ms.prev_link = pg_next.prev_link()
                ms.rnd = random.Random()
                ms.rnd.shuffle(ms.profile_links)
            resuming = True
        else:
            ms = MineState()
        def save():
            f = open(pickle_file, 'w+')
            pickle.dump(ms, f)
            f.close()
            print 'saving'
        while ms.current_url != None and ms.result_page_counter < 60: 
            pg = Page()
            pg.url = ms.root + ms.current_url
            pg.load()
            if resuming:
                print 'continuing here:'
                print ms.profile_links
                resuming = False
            else:
                ms.profile_links = pg.content_links()
                ms.profile_links_i = 0
                ms.profile_links_len = len(ms.profile_links)
                ms.next_link = pg.next_link()
                ms.prev_link = pg.prev_link()
                ms.rnd = random.Random()
                ms.rnd.shuffle(ms.profile_links)
                print 'shuffled'
                print ms.profile_links
            while ms.profile_links_i < ms.profile_links_len:
                if ms.profile_links_i + 1 == ms.profile_links_len:
                    print 'last'
                    time.sleep(2)

                if self.save_state:
                    print 'stopping at pl: %s %d, result_page: %d' %\
                        (ms.profile_links[ms.profile_links_i],\
                         ms.profile_links_i, ms.result_page_counter)
                    save()
                    sys.exit(0)
                # callback for profile processor
                time.sleep(1)
                ms.profile_links_i += 1
            ms.current_url = ms.next_link
            ms.result_page_counter += 1


def signal_handler(signal, frame):
   m.save_state = True

signal.signal(signal.SIGINT, signal_handler)

m = Mine()
m.run()
