import signal
import sys
import random
import os
import pickle

class MineState(object):
    def __init__(self):
        self.my_list = []
        self.my_list_len = 0
        self.i = 0

class Mine(object):
    def __init__(self):
        self.save_state = False
        pass

    def run(self):
        pickle_file = 'pickle_file'
        ms = None
        if os.path.isfile(pickle_file):
            f = open(pickle_file, 'r+')
            ms = pickle.load(f)
            f.close()
            ms.i += 1
            print 'loaded data, starting at: %d' % ms.i
        else:
            ms = MineState()
            ms.my_list = xrange(1, 200)
            ms.my_list_len = len(ms.my_list)

        import time
        while ms.i < ms.my_list_len:
            if m.save_state:
                print 'stopping at %d' % ms.i
                break
            time.sleep(1)
            ms.i += 1

        f = open(pickle_file, 'w+')
        pickle.dump(ms, f)
        f.close()
        print 'exiting'

def signal_handler(signal, frame):
   m.save_state = True

signal.signal(signal.SIGINT, signal_handler)

m = Mine()
m.run()
