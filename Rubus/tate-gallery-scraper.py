#!/bin/env python
# coding=utf-8

from bs4 import BeautifulSoup
import art_model as am
from art_model import ArtDatabase
import datetime
import pickle
import random, urllib2
import os
import signal
import socket
import string
import sys
import time

def my_print(x):
    # looks useless, but allows verbose mode to be toggled easily
    print x

def my_unicode(x):
    def to_unicode_or_bust(obj, encoding='utf-8'):
        if isinstance(obj, basestring):
            if not isinstance(obj, unicode):
                obj = unicode(obj, encoding)
        return obj
    return to_unicode_or_bust(x)

class Scraper(object):
    def __init__(self, site_id = u'untitled', res_directory='/home/tezl/Downloads'):
        self.res_directory = res_directory
        self.site_id = site_id
        self.user_agents = []
        self.openers = []
        self.load_user_agents()
        self.start_time = datetime.datetime.now()
        self.sleepy = 20
        self.root = 'http://www.tate.org.uk'
        self.g_try_again = 0

    def load_user_agents(self, filename = 'user_agents.txt'):
        try:
            fh = open(filename, 'r')
            self.user_agents = fh.readlines()
            for ua in self.user_agents:
                opener = urllib2.build_opener()
                opener.add_headers = [('User-agent', ua.strip())]
                self.openers.append(opener)
        except IOError:
            my_print('* * * ERROR: could not load user agents, defaulting to single')
            self.user_agents = ['Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36']
 
    def write_data(self, res, filename):
        import os
        sf = unicode(self.sanitize(filename))
        f = open(os.path.join(self.res_directory, sf), 'wb')
        f.write(res)
        f.close()
        return sf

    def make_absolute_url(self, url):
        if len(url) < 1: 
            print("url less than zero, strange...")
            return url
        if url.startswith('http://'):
            return url
        elif url.startswith('/'):
            return self.root + url
        else:
            return self.root + '/' + url
        return url

    def url_data(self, url, db_row_insert = True, try_again = 1):
        try:
            f_url = self.make_absolute_url(url)
            my_print("Downloading: %s" % (f_url,))
            opener = random.choice(self.openers)
            self.random_sleep()
            data = opener.open(f_url)
            data_read = data.read()
            return data_read
        except urllib2.HTTPError:
            my_print('HTTP error, ignoring %s' % f_url)
            return None
        except urllib2.URLError, socket.error:
            if self.g_try_again > 5:
                my_print('Something appears to be wrong with the internet connection, sleeping half an hour')
                time.sleep(1800)
                self.g_try_again = 0
            my_print('URL error, trying %s again in 20 seconds -- attempt %d' % (f_url, try_again))
            if try_again < 3:
                time.sleep(20)
                self.g_try_again += 1
                return self.url_data(url, db_row_insert, try_again + 1)
            else:
                self.g_try_again += 1
                return None
        return None

    def time_accessed(self):
        return datetime.datetime.now()

    def random_sleep(self, a = 3, b = 35):
        t = random.randrange(a, b)
        print('sleeping for %d seconds' % (t,))
        time.sleep(t)

    def sanitize(self, filename):
        filename = filename.replace(' ', '-')
        return u"".join([c for c in filename\
                    if c.isalpha() or c.isdigit() or c=='.' or c=='-' or c=='_']).strip()

    def scrape(self): pass

class PageState(object):
    def __init__(self):
        self.root = 'http://localhost:7000'
        self.current_url = '/'
        self.next_link = ''
        self.prev_link = ''
        self.result_page_counter = 0
        self.profile_links = []
        self.profile_links_i = 0
        self.profile_links_len = 0
        self.rnd = None 

def save(obj, pickle_file='pickle_file'):
    f = open(pickle_file, 'w+')
    pickle.dump(obj, f)
    f.close()

def restore(pickle_file='pickle_file'):
    f = open(pickle_file, 'r+')
    p = pickle.load(f)
    f.close()
    return p

class TateArtworkScraper(Scraper):
    def __init__(self, root = 'http://www.tate.org.uk',\
                    start = '/art/search?page=7&sort=acno&type=artwork'):
        super(TateArtworkScraper, self).__init__(\
                site_id = 'www.tate.org.uk',\
                res_directory='/home/tezl/Projects/Tecomella/artcollection')
        self.art_db = ArtDatabase('art.db')
        self.art_db.connect()
        self.root = root
        self.start = start
        self.save_state = False

    def load_page_soup(self, url):
        ud = self.url_data(url, False)
        if ud is None: return BeautifulSoup()
        return BeautifulSoup(ud)
        
    def content_links(self, sp):
        gwi = sp.find_all('div', class_='grid-item-image')
        if gwi is None: return []
        o = [] 
        for g in gwi:
            if g.a is None: continue
            href = g.a.get('href')
            if href is None: continue
            o.append(href)
        return o

    def next_link(self, sp):
        pager_next = sp.find('li', class_='pager-next')
        if pager_next != None:
            return pager_next.find('a').get('href')
        return None

    def prev_link(self, sp):
        pager_prev = sp.find('li', class_='pager-previous')
        if pager_prev != None:
            return pager_prev.find('a').get('href')
        return None

    def scrape(self):
        pickle_file = 'pickle_file'
        soup = None
        ps = PageState() 
        resuming = False
        def load_from_soup(sp):
            p = PageState()
            p.profile_links = self.content_links(sp) 
            p.profile_links_i = 0
            p.profile_links_len = len(p.profile_links)
            p.next_link = self.next_link(sp)
            p.prev_link = self.prev_link(sp)
            p.rnd = random.Random()
            p.rnd.shuffle(p.profile_links)
            p.root = self.root
            p.current_url = self.start
            return p
        if os.path.isfile(pickle_file):
            ps = restore(pickle_file)
            if ps.profile_links_i + 1 < ps.profile_links_len:
                ps.profile_links_i += 1
            else:
                soup_a = self.load_page_soup(ps.current_url)
                ps.current_url = self.next_link(soup_a)
                soup_b = self.load_page_soup(ps.current_url)
                ps = load_from_soup(soup_b)
                ps.result_page_counter += 1
            resuming = True
        else:
            ps = PageState()
            ps.current_url = self.start
        soup = None
        ps.root = self.root
        while ps.current_url != None:
            print ('starting new page %s' % ps.current_url)
            soup = self.load_page_soup(ps.current_url)
            if resuming: resuming = False
            else: 
                rc = ps.result_page_counter
                ps = load_from_soup(soup)
                ps.result_page_counter = rc 
            assert len(ps.profile_links) > 0
            while ps.profile_links_i < ps.profile_links_len:
                print('profile %d out of %d' % (ps.profile_links_i + 1, ps.profile_links_len))
                if (datetime.datetime.now() - self.start_time).seconds > self.sleepy:
                    self.random_sleep()
                    self.sleepy = random.randrange(6, 30)
                lk = ps.profile_links[ps.profile_links_i]
                aw = self.scrape_artwork_profile(lk)
                self.art_db.insert_artwork(aw['artwork'],\
                                           aw['artwork_summary'],\
                                           aw['artist'])
                if self.save_state:
                    print('stopping at position: %s %d, result page: %d' %\
                                (lk,
                                 ps.profile_links_i,\
                                 ps.result_page_counter))
                    save(ps)
                    sys.exit(0)
                ps.profile_links_i += 1
            ps.current_url = ps.next_link
            ps.result_page_counter += 1
            ps.profile_links_i = 0
            ps.profile_links = []
            ps.profile_links_len = 0
            ps.root = self.root
        self.art_db.close()

    def scrape_artwork_profile(self, url):
        page_data = self.url_data(url) 
        if page_data is None: # maybe something went wrong, try again
            page_data = self.url_data(url)
            if page_data is None: return u''
        soup = BeautifulSoup(page_data)
        caption = soup.find('div', class_='caption')
        def get_caption_property(p):
            if caption is None: return u''
            p_span = caption.find('span', class_=p)
            if p_span is not None:
                if p_span.string is not None:
                    return unicode(p_span.string)
            print('failed to get %s' % p)
            return u''

        artist = am.make_artist()
        artwork = am.make_artwork()
        artwork_summary = am.make_artwork_summary()
        artist['name'] = my_unicode(get_caption_property('artist'))
        artwork['title'] = my_unicode(get_caption_property('title'))
        import datetime
        def get_artwork_date():
            try:
                du = my_unicode(get_caption_property('date'))
                du.strip()
                if du.isnumeric():
                    return datetime.datetime.strptime(du, '%Y')
            except ValueError:
                return None
            return None
        artwork['date_produced'] = get_artwork_date()

        def get_life_dates(sp):
            try:
                iwad = sp.find('span', class_='infoWorkArtDates')
                if iwad is None: return (None, None)
                if u'–' in iwad.string:
                    ps = iwad.string.split(u'–')
                    start = ps[0].strip()
                    end = ps[1].strip()
                    return (datetime.datetime.strptime(start, '%Y'),\
                            datetime.datetime.strptime(end, '%Y'))
            except ValueError:
                return (None, None)
            return (None, None)

        artist['start_date'], artist['end_date'] = get_life_dates(soup)

        def save_image(sp):
            image_footer = sp.find('div', class_='image_footer')
            if image_footer is None:
                return u''
            image_a = image_footer.find('a', text='Full screen')
            if image_a == None:
                return u''
            image_link = image_a.get('href')
            image_data = self.url_data(image_link)
            if image_data is None: return u''
            ext = image_link.split('/')[-1].split('.')[1]
            bf = u'%s-%s' % (artist['name'].lower(), artwork['title'].lower())
            bf = bf[:64] # prevent IOError because of long filenames
            suggested_filename = u'%s-%d.%s' % (bf,\
                                               random.randrange(1, 10000),\
                                               ext)
            filename = self.write_data(image_data, suggested_filename)
            return filename

        artwork['filename'] = save_image(soup)
        def get_properties(sp):
            def get_property(span_lvl, span_dict, sib_levl, sibling_dict):
                p_span = sp.find(span_lvl, **span_dict)
                if p_span == None:
                    return u''
                next_sib = p_span.find_next_sibling(sib_levl, **sibling_dict)
                if next_sib.string != None:
                    return next_sib.string
                elif next_sib.strings != None:
                    return u'\n'.join([s for s in next_sib.strings])
                elif next_sib.contents != None:
                    return next_sib.contents
                else:
                    return u''
            details = []
            for p in ['Medium', 'Dimensions', 'Acquisition']:
                details.append('%s: %s' % (p, get_property('span',\
                                { 'class_':'infoLabel', 'text':p },\
                                'span', {})))

            details.append('%s: %s' % ('Collection', get_property('span',\
                                {'class_':'infoLabel', 'text':'Collection'},\
                                'div', {'class_':'infoValue'})))
            return u'\n'.join(details)

        artwork['details'] = get_properties(soup) 
        def get_reference(sp):
            # of course, an annoying edge case
            reference_span = sp.find('span', class_='infoLabel',\
                                        text='Reference')
            if reference_span == None: return u''
            ref_nex_sib = reference_span.find_next_sibling('div')
            if ref_nex_sib == None: return u''
            ref_span = ref_nex_sib.find('span')
            if ref_span == None: return u''
            ref_span_span = ref_span.span
            if ref_span_span == None: return u''
            if ref_span_span.string == None: return u''
            return my_unicode(ref_span_span.string.strip())

        artwork['reference'] = get_reference(soup)
        def get_summary(sp):
            def uj(x):
                if x is None: return u''
                return u'\n'.join([my_unicode(a.__repr__())\
                                        for a in x.contents])
            summary_div = sp.find('div', class_='research-read-more')
            if summary_div is not None:
                if summary_div.a is None: return u''
                summary_link = summary_div.a.get('href')
                # annoying bug in their website
                if summary_link.startswith('http://localhost')\
                    or summary_link.startswith('https://localhost'): return u''
                # deals with href=""
                if len(summary_link) < 1: return u''
                summary_page_data = self.url_data(summary_link)
                if summary_page_data is None: return u''
                sysp = BeautifulSoup(summary_page_data)
                article = sysp.find('article')
                return uj(article)
            summary_div = sp.find('div', class_='texts_content')
            if summary_div is not None:
                return uj(summary_div)
            return u''
        artwork_summary['summary'] = get_summary(soup)
        return {'artwork':artwork,
                'artist':artist,\
                'artwork_summary':artwork_summary}

if __name__ == '__main__':
    aws = TateArtworkScraper()
    def signal_handler(signal, frame): aws.save_state = True
    signal.signal(signal.SIGINT, signal_handler)
    aws.scrape()

