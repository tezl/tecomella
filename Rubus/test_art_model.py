from art_model import ArtDatabase, make_artwork, make_artist, make_artwork_summary

G_art_db = None
import datetime as dt

if __name__ == '__main__':
    G_art_db = ArtDatabase()
    G_art_db.connect()

    artwork = make_artwork(title='apple', date_produced=dt.date(1989, 1, 1), reference='ekje98', filename='apple.jpg', details='more details')
    artist = make_artist(name='fred', sort_name='fred', start_date=dt.date(1932, 1, 2), end_date=dt.date(1994, 1, 2))
    artwork_summary = make_artwork_summary(summary='hello')

    G_art_db.insert_artwork(artwork, artwork_summary, artist)

    rt = G_art_db.select_artwork(1)
    print(rt)
    print

    rt = G_art_db.select_artwork_by_title('apple')
    print(rt)
    print

    rt = G_art_db.select_artworks_by_artist_name('fred')
    print(rt)
    print

    rt = G_art_db.select_artworks_by_reference('ekje98')
    print(rt)
    print

    G_art_db.delete_artwork(1)

    G_art_db.delete_artist(1)


    newartist = make_artist(name='bob jones', sort_name='bob jones', start_date=dt.date(1920, 1, 4), end_date=dt.date(1923, 5, 4))
    rt = G_art_db.insert_artist(newartist)
    nat = G_art_db.select_artist(rt)
    print(nat)
    print()

    nat = G_art_db.select_artist_by_name('bob')
    print(nat)
    print()

    nat = G_art_db.select_artists_by_period(dt.date(1919, 1, 1), dt.date(2000, 1, 1))
    print(nat)
    print()

    G_art_db.delete_artist(rt)


    x = G_art_db.insert_artwork_summary({'artwork_summary_id':None, 'summary':'hello hello hello'})
    aws = G_art_db.select_artwork_summary(x)
    G_art_db.delete_artwork_summary(aws['artwork_summary_id'])

    G_art_db.close()

