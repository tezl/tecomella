from bottle import route, run, template, redirect

@route('/<o>.html')
def detail(o):
    return """<doctype html>
                <html>
                    <head><title>%s</title></head>
                    <body>this is %s.html</body>
                </html>""" % (o, o)
@route('/')
@route('/<page:int>')
def index(page=0):
    nxt = 0
    prv = 0
    if page > 10:
        redirect('/')
    if page == 10: 
        nxt = -1
    else:
        nxt = page + 1
    if page < 1:
        prv = -1
    else:
        prv = page - 1
    import os, binascii
    c = 0
    links = []
    while c < 20:
        r = binascii.b2a_hex(os.urandom(10))
        links.append(('%s.html' % r, r))
        c += 1
    return template('index.html', prv=prv, nxt=nxt, links=links)
run(host='localhost', port=8080, reloader=True)
