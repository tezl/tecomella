#!/usr/bin/env python

import random
import bs4, codecs, locale, urllib2, random, requests, string, sys
import os
import os.path
import urlparse

sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout) 

class UAGen(object):
    def __init__(self, filename='user_agents.txt'):
        self.user_agents = []
        self.filename = filename 
    def load(self):
        try:
            fh = open(self.filename, 'r')
            self.user_agents = fh.read().split('\n')
        except IOError:
            print(u'# failed loading user agents file')
            self.user_agents = ['Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36']
    def get(self):
        return random.choice(self.user_agents)

def rsleep(a=0, b=30):
    return u''
    return u'sleep %d ;' % (random.randrange(a, b))

G_ua_gen = None 

def date_accessed():
    return u"$(date +'%F %H:%M:%S')"

def content_hash(filename):
    return u"$(openssl dgst -sha256 '%s' |  sed 's/^.\+= //g')" % (filename,)

class CarlinaClient(object):
    def __init__(self, server, port):
        self.server = server
        self.port = port

    def basic_client(self):
        return u'carlina_client --server "%s" --port "%s" ' % (self.server, self.port)

    def register(self):
        return u'export CARLINA_SESS_ID=$(carlina_client --server "%s" --port "%s" register);'  % (self.server, self.port)

    def add(self, url, content_hash, title):
        site = urlparse.urlparse(url).netloc 
        title = title.replace('"', '\\"')
        title = title.replace('`', '\\`')
        title = title.replace("$", '\\$')
        return u'%s add --sess-id "$CARLINA_SESS_ID" --site "%s" --url "%s" --title "%s" --content-hash "%s" --date-accessed "%s" ;'\
                 % (self.basic_client(), site, url, title, content_hash, date_accessed())

    def remove(self, page_id):
        return u'%s remove --sess-id "$CARLINA_SESS_ID" --page-id "%s" ;'  % (self.basic_client(), page_id,)

    def update(self, page_id, content_hash):
        return u'%s update --sess-id "$CARLINA_SESS_ID" --page-id "%s" --content_hash "%s" --date_accessed "%s" ;'\
                    % (self.basic_client(), page_id, content_hash, date_accessed())

    def find_by_url(self, url):
        return u'%s find --sess-id "$CARLINA_SESS_ID" --raw --by-url "%s"' % (self.basic_client(), url)

def var_enclose(varname, cmd):
    return u'%s=$(%s);' % (varname, cmd)

def wget(url, out_doc, title=''):
        return u'wget --user-agent="%s" --output-document="%s" "%s" ;' %  (G_ua_gen.get(), out_doc, url)

def phantomjs(url, out_doc, title=''):
        return u'timeout -k 5m -s SIGINT 5m capturejs -V 1280x720 -I -u "%s" -o "%s" ;' % (url, out_doc)

def youtubedl(url, out_doc, title=''):
    return u'youtube-dl -q --no-playlist --max-filesize 500m --output "%s" "%s" ;' % (out_doc, url)

def guesser(url, out_doc, title=''):
    l_url = url.lower()
    if '?' in l_url:
        l_url =l_url.split('?')[0]
    if 'imgur' in l_url:
        return wget(url, out_doc)
    if ('youtube' in l_url) or ('youtu.be' in l_url)\
            or ('vimeo' in l_url) or ('video' in title.lower()): 
        return youtubedl(url, out_doc)
    exts = ['.pdf', '.jpg', '.png', '.gif', '.docx', '.doc', '.mp3']
    for e in exts:
        if e in l_url:
            return wget(url, out_doc)
    return phantomjs(url, out_doc)

def if_wrap(condition, fn, else_fn=None):
    if_con = u'if [ %s ]; then ' % (condition)
    if_end = u'fi;'
    if else_fn is None:
        return [
            if_con,
            fn(),
            if_end
        ];
    else:
        return [if_con,
                fn(),
               u'else',
                else_fn(),
                if_end]

def cmd(cc, url, out_fname, title, fn):
    out = []
    rt = fn(url, out_fname, title)
    if len(rt) > 0:
        out.append(var_enclose('url_found', cc.find_by_url(url)))
        out.extend(
                   if_wrap(u'"%s" == ""' % u'$url_found',
                    
                   lambda:
                    "\n".join(["echo 'getting %s to %s' ;" % (url, out_fname),\
                              rt]\
                    + if_wrap(\
                    u'-f "%s"' % (out_fname),
                    lambda: cc.add(url, content_hash(out_fname), title))
                    )
                   )
                   )
        out.append(rsleep())
    return out

def go(its, cmd, args):
    out = []
    cc = CarlinaClient(server=args.server,port=args.port)
    out.append(u'#!/bin/bash')
    out.append(cc.register())

    def extender(x):
        if x.has_key('url') and x.has_key('filename'):
            u = x['url']
            f = x['filename']
            t = x['title']
            if (len(u) > 4) and (len(f) > 1):
                out.extend(cmd(cc, u, f, t))
    for i in its:
        if type(i) is dict:
            if i.has_key('group'):
                for j in i['group']:
                    extender(j)
            else:
                extender(i)
    print(u'\n'.join(out))

if __name__ == '__main__':
    import argparse
    import json
    inp = sys.stdin.read()
    my_fn= None
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--command', help="download command option [wget, phantomjs, youtube-dl, guess]", type=str, default=u'guess')
    parser.add_argument('-d', '--directory-prefix', help="download directory prefix", type=str, default=u'')
    parser.add_argument('-s', '--server', help='server running carlina', type=str, required=True)
    parser.add_argument('-p', '--port', help='port of server running carlina', type=str, required=True)
    parser.add_argument('-u', '--user-agents-file', help='file containing list of user agents', type=str, required=False)
    args = parser.parse_args()
    server = args.server
    port = args.port
    c = args.command
    p = args.directory_prefix
    user_agents_file = args.user_agents_file
    if user_agents_file is not None:
        G_ua_gen = UAGen(user_agents_file)
    else:
        G_ua_gen = UAGen('user_agents.txt')
    
    def mk_path(a, b): return os.path.join(a, b)
    lkp  = { 'wget':lambda cc, x, y, t: cmd(cc, x, mk_path(p, y), t, wget),
             'phantomjs':lambda cc, x, y, t: cmd(cc, x, mk_path(p, y), t, phantomjs),
             'youtube-dl':lambda cc, x, y, t: cmd(cc, x, mk_path(p, y), t, youtubedl),
             'guess':lambda cc, x, y, t: cmd(cc, x, mk_path(p, y), t, guesser)}
    if lkp.has_key(c):
        my_fn = lkp[c]
    else:
        print('* * * ERROR: command not recognized')
        sys.exit(1)
    try:
        its = json.loads(inp)
        G_ua_gen.load()
    except ValueError:
        print('* * * ERROR: invalid JSON input')
        sys.exit(1)
    go(its, my_fn, args)

