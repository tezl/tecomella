#!/usr/bin/python

# 
# Izaak 2014
#

import bs4, codecs, locale, urllib2, random, requests, string, sys

sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout) 

def pdfrip(root, relurl):
    data = requests.get(root + relurl)
    soup = bs4.BeautifulSoup(data.content)
    got_pdfs = []
    lks = soup.find_all('a')
    for l in lks:
        href = l.get('href')
        if href == None: continue
        if 'pdf' in href.lower():
            l_f = ''
            if href.startswith('http://') or href.startswith('https://'):
                l_f = href
            else:
                if href.startswith('/'):
                    l_f = root + href
                else:
                    l_f = root + '/' + href
            if l_f not in got_pdfs:
                filename = href.split('/')[-1]
                sane = u"".join([x if (x.isalnum() or x == '.') else "-" for x in filename])
                print u'wget --output-document="%s" "%s" ; sleep %d ;' % (sane, l_f, random.randrange(5, 20))
                got_pdfs.append(l_f)
            else: continue

if __name__ == '__main__':
    import sys
    print '#!/bin/bash'
    pdfrip(sys.argv[1], sys.argv[2])

