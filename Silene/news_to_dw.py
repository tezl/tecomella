import sys
import json

if __name__ == '__main__':
    inp = sys.stdin.read()
    try:
        its = json.loads(inp)
    except ValueError:
        print('* * * ERROR: invalid JSON input')
        sys.exit(1)

    def title_to_filename(t):
        t = t.lower()
        return u"".join([x if (x.isalnum() or x == '.') else "-" for x in t])

    out = []
    lks = its['links']
    for i in lks:
        if not (i.has_key('article_url') and\
                i.has_key('comment_url') and\
                i.has_key('title')): continue
        fname = lambda y: u'%s-%s.pdf' %\
                        (title_to_filename(i['title']), y)
        out.append({'group':[{'url': i['article_url'],\
                              'filename':fname('article')},\
                              {'url': i['comment_url'],\
                               'filename':fname('comments') }]})

    print(json.dumps(out))

