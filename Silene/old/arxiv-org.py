import bs4
import datetime
import random
import requests
import string

ROOT = 'http://arxiv.org'

def clean(s):
    return ''.join([p for p in s.strip().lower() if p in\
        string.ascii_letters + string.digits + ' ']).replace(' ', '-')

def extract(soup):
    out = []
    a = soup.find_all('a', title='Download PDF')
    for x in a:
        y= x.find_parent('dt')
        if y is None: continue
        sib = y.find_next_sibling('dd')
        if sib is None: continue
        d = sib.find('div', class_='list-title')
        if d is None: continue
        if len(d.contents) < 3: continue
        title = clean(d.contents[2])
        out.append({'href':x.get('href'), 'title':title})
    return out

def gen_sh(link, outdir='out'):
    pg = requests.get(link)
    soup = bs4.BeautifulSoup(pg.content)
    links = extract(soup)
    uf = open('useragents.txt')
    rs = [r.strip() for r in uf.readlines()]
    r = random.choice(rs)
    uf.close()
    print("mkdir %s;" % outdir)
    for l in links:
        print('wget --user-agent="%s" --output-document="%s/%s" %s ; sleep %d;'\
            % (r, outdir, l['title'] + '.pdf', ROOT + l['href'], random.randrange(30, 120))) 

if __name__ == '__main__':
    d=datetime.datetime.now().strftime("%Y-%m-%d")
    gen_sh('http://arxiv.org/list/cs/new?skip=0&show=100', 'cs-%s' % d)
    gen_sh('http://arxiv.org/list/math/new?skip=0&show=100', 'math-%s' % d)

