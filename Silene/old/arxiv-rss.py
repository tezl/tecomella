
import feedparser
import bs4
import requests
import re
import random
import datetime
import string
import os
import os.path

ROOT = 'http://www.arxiv.org'

def find_pdf(pg):
    sp = bs4.BeautifulSoup(pg)
    d = sp.find(text=re.compile('PDF.*'))
    if d is not None and d.parent is not None:
        return d.parent.get('href')
    return None

def go():
    feed = feedparser.parse('http://export.arxiv.org/rss/cs.CL')
    def ct(l):
        g = requests.get(l)
        return g.content
    lks  = [{ 'link':f['link'], 'title':f['title'] } for f in feed.entries ]
    pdfs = [ {'link':find_pdf(ct(l['link'])), 'title':l['title']} for l in lks ]
    print out(pdfs)

def out(ps):
    def clean(s):
        return ''.join([p for p in s.strip().lower() if p in\
                string.ascii_letters + string.digits + ' ']).replace(' ', '-')
    d=datetime.datetime.now().strftime("%Y-%m-%d")
    feeds = [ {'name':'cs-CL', 'url':'http://export.arxiv.org/rss/cs.CL' },
              {'name':'cs-CR', 'url':'http://export.arxiv.org/rss/cs.CR' },
              {'name':'cs-AR', 'url':'http://export.arxiv.org/rss/cs.AR' },
              {'name':'cs-OS', 'url':'http://export.arxiv.org/rss/cs.OS' },
              {'name':'cs-NI', 'url':'http://export.arxiv.org/rss/cs.NI' },
              {'name':'cs-DC', 'url':'http://export.arxiv.org/rss/cs.DC' },
              {'name':'cs-DS', 'url':'http://export.arxiv.org/rss/cs.DS' },
              {'name':'q-bio.NC', 'url':'http://export.arxiv.org/rss/q-bio.NC'},
              {'name':'q-bio.GN', 'url':'http:/export.arxiv.org/rss/q-bio.GN'},
              {'name':'q-bio.MN', 'url':'http:/export.arxiv.org/rss/q-bio.MN'},
              {'name':'math.MP', 'url':'http://export.arxiv.org/rss/math.MP'},
              {'name':'math.GM', 'url':'http://export.arxiv.org/rss/math.GM'},
              {'name':'math.RA', 'url':'http://export.arxiv.org/rss/math.RA'},
              {'name':'math.QA', 'url':'http://export.arxiv.org/rss/math.QA'},
              {'name':'math.NT', 'url':'http://export.arxiv.org/rss/math.NT'},
              {'name':'math.AG', 'url':'http://export.arxiv.org/rss/math.AG'},
              {'name':'math.AT', 'url':'http://export.arxiv.org/rss/math.AT'},
              {'name':'math.GT', 'url':'http://export.arxiv.org/rss/math.GT'}
              ]
    cd = 'arxiv-%s' % (d)
    out = ['mkdir %s' % (cd) ] 
    ua = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
    for f in feeds:
        fd = f['name']
        out.append('mkdir %s/%s' % (cd, fd))
        for p in ps:
            od = os.path.join(cd, fd, '%s.pdf' % clean(p['title']))
            dw_cmd = "wget --user-agent='%s' --output-document='%s' %s%s" % (ua, od, ROOT, p['link'])
            out.append(dw_cmd)
            rnd = 'sleep %d'  % random.randrange(5, 30)
            out.append(rnd)
    return ';'.join(out)

if __name__ == '__main__':
    go()

