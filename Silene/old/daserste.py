#!/bin/env python
# -*- coding: utf-8 -*-
import bs4
import datetime 
import requests
import argparse
import sys
import json
import codecs, locale

sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout) 
SITE_ROOT='http://mediathek.daserste.de'

def main(dt):
    stdt = dt.strftime('%d.%m.%Y')

    page = requests.get(SITE_ROOT + '/sendungVerpasst', params={'datum':stdt,})
    soup = bs4.BeautifulSoup(page.content, 'lxml')
    al = soup.find_all('a', class_='textLink')
    out = []
    for a in al:
        href = a.get('href')
        if href is None: continue
        if '20-00-Uhr' in href:
            out.append(href)
    lks = [] 
    for n in out:
        lks.append({u'url':u"%s%s" % (SITE_ROOT,n),
         u'title':u'Das Erste Tageschau %s'  % (stdt,),
         'filename':u'das-erste-tageschau-%s.mp4' % (stdt,)})
    print(json.dumps(lks, ensure_ascii=False))
     
if __name__ == '__main__': 
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--date', help="retrieve news on particular date -- format: YYYY-mm-dd", type=str, required=True)

    args = parser.parse_args()
    try:
        dt = datetime.datetime.strptime(args.date, '%Y-%m-%d')
    except ValueError:
        parser.error('invalid date format -- use: YYYY-mm-dd')
        sys.exit(1)

    main(dt)
