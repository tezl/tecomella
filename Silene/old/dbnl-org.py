import bs4
import bs4
import random
import requests
import string
import sys
import time


ROOT = 'http://www.dbnl.org'

def getpage(lk):
    global ROOT
    gt = ROOT + lk
#    print 'get: %s' % lk
    s = random.randrange(2, 6) 
   # time.sleep(s)
    pg = requests.get(gt, headers={'User-Agent':'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0'}, allow_redirects=True)
    print pg.content[:100]
    if 'Location' in pg.content:
        # try again
        print 'trying again'
        pg = requests.get(ROOT + pg.headers.split(' ')[1])
    if pg is not None: return pg.content
    return None
pg = getpage('/titels/index.php?s=genre')
sp = bs4.BeautifulSoup(pg)

def find_genre_h4(sp, genre):
    tg = sp.find_all('h4')
    for t in tg:
        f = t.find('font')
        if f is None: continue
        if f.string is None: continue
        if len(t.contents) >= 1:
            if genre.lower() == (t.contents[0] + f.string).lower():
                return t
    return None

genre_h4 = find_genre_h4(sp, 'drama - abel spel')

sib = genre_h4.find_next_sibling()
alks = []
while sib is not None and sib.name != 'h4':
    a = sib.find('a')
    if a is not None:
        hr = a.get('href')
        if hr is not None:
            alks.append(hr)
    sib = sib.find_next_sibling()

dllks = []
for a in alks:
    pg = getpage(a) 
    sp = bs4.BeautifulSoup(pg)
    h4 = sp.find('h4', text='Beschikbare tekst in de dbnl')
    b = h4.find_next_sibling()
    href = b.get('href')
    if href is None: 
        print 'skipping %s' % str(a)
        continue
    dllks.append(href) 

def get_bekijk(s):
    if s is None: return False
    c = ''.join(s.stripped_strings)
    return 'bekijk' == c 
#print dllks
pdfs = []
#dllks = ['/tekst/_glo001glor01_01']
#for d in dllks:
#    print d
#    pg = getpage(d + '/downloads.php')
#    print 'error' 
#    if pg == None: continue # 404 no downloads
#    print 'download page exists'
##    sp = bs4.BeautifulSoup(pg)
#    pdfa = sp.find(get_bekijk)
#    print 'finding bekijk'
#    if pdfa is None: 
#        print 'finding original scans'
#        sc = sp.find('h4', text='scans van originelen')
#        if sc is None: continue
#        n = sc.find_parent('a').find_next_sibling()
#        if n is not None:
 #           pdfa = n
 #       else: continue
#    print 'finding pdfa'
#    href = pdfa.get('href')
#    if href is None: continue
#    print 'appending'
 #   pdfs.append(d + href[1:])
#print pdfs
