import codecs
import locale
import sys

# Wrap sys.stdout into a StreamWriter to allow writing unicode.
sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout) 

import bs4
import random
import requests
import string
import time

ROOT = 'https://defcon.org'

def getpage(lk):
    global ROOT
    gt = ROOT + lk
    s = random.randrange(2, 6) 
    time.sleep(s)
    pg = requests.get(gt, headers={'User-Agent':'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0'})
    if pg is not None: return pg.content
    return None
pg = getpage(sys.argv[1])
sp = bs4.BeautifulSoup(pg)
talks = sp.find_all('div', class_='talk2')
tls = []
def gen_title():
    return ''.join(random.choice('0123456789ABCDEF') for i in range(16))
for t in talks:
    p = t.find('p')
    if p is None: continue
    p = p.find_next_sibling('p')
    if p is None: continue
    title = gen_title() 
    tdom = t.find(class_='talkTitle')
    if tdom is not None:
        if tdom.string is not None:
            title = tdom.string
    aks = p.find_all('a')
    pdfs = []
    slides_video_href = u''
    audio_href = u''
    for a in aks:
        href = a.get('href')
        if href == None: continue
        if a.string == None: continue
        la = a.string.lower()
        if la == 'slides video' or la == 'slides only':
            slides_video_href = href 
        if 'm4b' in href:
            audio_href = href 
        if 'pdf' in href:
            tr = href[8:]
            if tr not in pdfs:
                pdfs.append(unicode(tr))
    if len(pdfs) > 0:
        slides_video_href = ''
    if len(slides_video_href) > 0 and len(audio_href) > 2:
        audio_href = ''
    if slides_video_href == '' and audio_href == '' and pdfs == []:
        continue
    title = u''.join([x for x in title if x in string.ascii_letters + string.digits + ' ']).lower().replace(' ', '-')
    tls.append({'video':unicode(slides_video_href),\
                'audio':unicode(audio_href),\
                'pdf':pdfs,
                'title':unicode(title)})
def mkdir(d):
    v = u'mkdir "%s" ;'
    print v % d

def wget(d, f, l):
    v = u'wget --output-document="%s/%s" --wait 3 --random-wait --user-agent="Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0" "%s" ;' 
    print v % (d, f, l)
    print u'sleep %d ;' % random.randrange(2, 6)

for t in tls:
    d = t['title'] 
    mkdir(d)
    if len(t['video']) > 0:
        wget(d, unicode(gen_title() + '.m4v'), t['video'])
    if len(t['audio']) > 0:
        wget(d, unicode(gen_title() + '.m4b'), t['audio'])
    for p in t['pdf']:
        wget(d, unicode(gen_title() + '.pdf'), ROOT + p)

