import bs4
import random
import requests
import sys
import time
def getpage(lk):
    ROOT = 'http://www.freebookcentre.net'
    gt = ROOT + lk
    s = random.randrange(2, 6) 
    print 'sleeping %d' % s
    time.sleep(s)
    print 'GET %s' % gt
    pg = requests.get(gt, headers={'User-Agent':'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0'})
    if pg is not None: return pg.content
    return None

def get_category(cat):
    print 'getting category %s' % cat
    pg = getpage(cat)
    if pg is None: return
    soup = bs4.BeautifulSoup(pg)
    bg = soup.find('td', class_='CategoryBg') # stop at the first one with .find 
    cs = bg.find_all('a')
    bk_lks = []
    for c in cs:
        href = c.get('href')
        if href is None: continue
        np = getpage(href)
        if np is None: continue
        ns = bs4.BeautifulSoup(np)
        bks = ns.find_all('div', itemtype='http://schema.org/Book')
        for b in bks:
            a = b.find('a')
            if a is None: continue
            href = a.get('href')
            if href is None: continue
            bk_lks.append(href[2:])
    dl_lks = []
    ext_lks = []
    for b in bk_lks:
        pg = getpage(b)
        bp = bs4.BeautifulSoup(pg)
        header = bp.find('td', class_='HeaderContentBg LRHeaderBorder LeftPad')
        def gen_title():
            return ''.join(random.choice('0123456789ABCDEF') for i in range(16))
        title = gen_title() 
        if header is not None:
            hc = header.contents
            if hc is not None and len(hc) >= 3:
                if 'Template' not in hc[2]:
                    title = unicode(hc[2]).replace(' ', '-')
        dl = bp.find('td', class_='Downloadlink')
        if dl is None: continue
        a_dl = dl.find('a')
        if a_dl is None: continue
        href = a_dl.get('href')
        if href is None: continue
        if 'PDF' in pg: dl_lks.append((title, href))
        else: ext_lks.append((title, href))
    return {'pdf':dl_lks, 'external':ext_lks}

if __name__ == '__main__': 
    dl = get_category(sys.argv[1])
    def pdf_lks(): 
        for d in dl['pdf']: print d
    def ext_lks(): 
        for e in dl['external']: print e
    if len(sys.argv) == 3:
        if sys.argv[2] == 'pdf': pdf_lks() 
        elif sys.argv[2] == 'ext': ext_lks() 
    else:
        print 'PDF'
        pdf_lks()
        print 'EXTERNAL'
        ext_lks()

