var page = require('webpage').create(), system = require('system');

if (system.args.length != 3) {
        console.log('Usage: get_snap.js url file');
            phantom.exit(1);
} 
else {
      var url = system.args[1]
        var file = system.args[2]

          page.viewportSize = { width: 1024, height: 768 }; 

            page.open(url, function() {
                    page.clipRect = { top: 0, left: 0, width: 1024, height: 768 };
                        page.render(file);
                            phantom.exit();
                              });
}

