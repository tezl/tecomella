
JURN_DIRECTORY='http://www.jurn.org/directory/'

def read_directory():
    f = open('directory.html')
    return f.read()

def make_soup():
    import bs4
    pg_data = read_directory()
    return bs4.BeautifulSoup(pg_data)

def fn_cat(cat):
    soup = make_soup()
    lc = soup.find('h3', class_='toggler',
                        text=cat)
    print cat
    print
    div = lc.find_next_sibling('div', class_='element')
    lks = div.find_all('a')
    for l in lks:
        print l.get('href')
    print

def fn_literature_literary_criticism():
    fn_cat('Literature : literary criticism')

def fn_literature_poetry_studies():
    fn_cat('Literature : poetry studies')

def fn_literature_small_press_magazines():
    fn_cat('Literature : small press magazines')

def fn_literature_in_translation():
    fn_cat('Literature : in translation')

def fn_religion_christian_denominations():
    fn_cat('Religion : Christian denominations')

def fn_religion_other():
    fn_cat('Religion : Buddhist, Jewish, Islamic, Hindu, Pagan')

def fn_history_asia_china_india_africa():
    fn_cat('History : Asia, China, India, Africa')

def fn_history_australasian():
    fn_cat('History : Australasian')

def fn_history_british_isles():
    fn_cat('History : British Isles')

def fn_history_north_america():
    fn_cat('History : North America')
    
def fn_history_classical_civilisations():
    fn_cat('History : classical civilisations')

def fn_music_musicology_music_education():
    fn_cat('Music : musicology and music education')

def fn_film_and_cinema():
    fn_cat('Film and cinema')

def fn_linguistics():
    fn_cat('Linguistics')

def fn_philosophy():
    fn_cat('Philosophy')

def fn_various_latin_america_africa_middle_east_india():
    fn_cat('Various : Latin America, Africa, Middle East, India')

def fn_various_far_northern_and_polar_regions():
    fn_cat('Various : far northern and polar regions')

def fn_various_europe():
    fn_cat('Various : Europe')

def fn_visual_arts_art_history():
    fn_cat('Visual Arts : art history')

if __name__ == '__main__':

    fns = [\
        fn_literature_literary_criticism,\
        fn_literature_poetry_studies,\
        fn_literature_small_press_magazines,\
        fn_literature_in_translation,\
        fn_religion_christian_denominations,\
        fn_religion_other,\
        fn_history_asia_china_india_africa,\
        fn_history_australasian,\
        fn_history_british_isles,\
        fn_history_north_america,\
        fn_history_classical_civilisations,\
        fn_music_musicology_music_education,\
        fn_film_and_cinema,\
        fn_linguistics,\
        fn_philosophy,\
        fn_various_latin_america_africa_middle_east_india,\
        fn_various_far_northern_and_polar_regions,\
        fn_various_europe,\
        fn_visual_arts_art_history ]

    for f in fns:
        f()

