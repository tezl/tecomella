SITE = 'http://www.lobste.rs'
import bs4
import json
import sys

def scrape(data):
    out = []
    soup = bs4.BeautifulSoup(data, "lxml")
    sts = soup.findAll('li', class_='story')
    for s in sts:
        comment_url = u''
        l = s.find('span', class_='link')
        if l is None: continue
        l_a = l.find('a')
        article_url = l_a.get('href')
        title = l_a.text
        c = s.find('span', class_='comments_label')
        if c is not None:
            a_c = c.find('a')
            if a_c is not None:
                comment_url = SITE + a_c.get('href')
        out.append({'title':title,
                    'article_url':article_url,
                    'comment_url':comment_url})
    return {'links':out}

if __name__ == '__main__':
    inp = sys.stdin.read()
    out = scrape(inp)
    print(json.dumps(out))
