SITE = 'http://news.ycombinator.com'
import bs4
import json
import sys

def scrape(data):
    out = []
    soup = bs4.BeautifulSoup(data)
    sts = soup.findAll('tr', class_='athing')
    for s in sts:
        td = s.find('td', class_='title')
        if td is None: continue
        sib = td.next_sibling
        if sib is None: continue
        sib = sib.next_sibling
        if sib is None: continue
        sib = sib.next_sibling
        if sib is None: continue
        a_s = sib.find('a')
        if a_s is None: continue
        article_url = a_s.get('href')
        title = a_s.text
        comment_url = td.find(text='comments')
        print 
        out.append({'title':title,
                    'article_url':article_url,
                    'comment_url':comment_url})
    return {'links':out}

if __name__ == '__main__':
    inp = sys.stdin.read()
    out = scrape(inp)
    print(json.dumps(out))

