#!/bin/env python
# -*- coding: utf-8 -*-
import bs4
import datetime 
import requests
import argparse
import sys
import json
import codecs, locale

sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout) 
SITE_ROOT='http://www.npo.nl'

def main(dt):
    stdt = dt.strftime('%d-%m-%Y')

    page = requests.get(SITE_ROOT + '/zoeken', params={'utf8':u'✓',\
                                                            'q':u'NOS Journaal',\
                                                            'sort_date[]':stdt,\
                                                            'sort_date_period':u'',\
                                                            'sort_date_period_start':u'',\
                                                            'sort_date_period_end':u'',\
                                                            'main_genre':u'',\
                                                            'sub_genre':u'',\
                                                            'broadcasters[]':u'NOS',\
                                                            'av_type':u'video'
                                                            })
    soup = bs4.BeautifulSoup(page.content, 'lxml')
    spans = soup.find_all('div', class_='span8')
    out = []
    for n in spans:
        h = n.find('h5')
        if h is not None and len(h.contents) > 0:
            if u'20:00' in h.contents[0]: # or u'18:00' in h.contents[0]:
                a = n.find('a')
                if a is not None: out.append(a.get('href'))
    lks = [] 
    for n in out:
        lks.append({u'url':u"%s%s" % (SITE_ROOT,n),
         u'title':u'NOS Journaal %s'  % (stdt,),
         'filename':u'nos-journaal-%s.mp4' % (stdt,)})
    print(json.dumps(lks, ensure_ascii=False))
     
if __name__ == '__main__': 
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--date', help="retrieve news on particular date -- format: YYYY-mm-dd", type=str, required=True)

    args = parser.parse_args()
    try:
        dt = datetime.datetime.strptime(args.date, '%Y-%m-%d')
    except ValueError:
        parser.error('invalid date format -- use: YYYY-mm-dd')
        sys.exit(1)

    main(dt)
