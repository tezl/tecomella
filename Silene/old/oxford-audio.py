#!/usr/bin/env python

#
# Izaak 2014
#

import bs4, os, urllib2

G_ROOT = 'http://podcasts.ox.ac.uk'
def write_data(res, filename):
    print 'Writing: %s' % filename
    f = open(filename, 'wb')
    f.write(res)
    f.close()

def get_url_data(url):
    global G_ROOT
    try:
        print "Downloading: %s" % (url,)
        data = urllib2.urlopen(url)
        return data.read()
    except urllib2.HTTPError:
        print 'HTTP error, ignoring'
        return None

def scrape_listing(page):
    global G_ROOT
    soup = bs4.BeautifulSoup(page)
    episodes = soup.find_all('td', class_='views-field-title')
    o_lks = []
    for e in episodes:
        lk = e.find('a')
        if lk != None:
            href = lk.get('href')
            if href != None:
                o_lks.append(G_ROOT + href)
    return o_lks

def scrape_audio(page):
    soup = bs4.BeautifulSoup(page)
    lks = soup.find_all('a', text='Audio')
    au_lk = None
    for l in lks:
        href = l.get('href')
        if href.endswith('mp3'):
            return href
    return None

def download_series(url):
    def random_sleep(a = 8, b = 25):
        import time, random
        time.sleep(random.randrange(a, b))
    listing_data = get_url_data(url)
    if listing_data == None:
        print 'Series not found'
        return
    random_sleep()
    episodes = scrape_listing(listing_data)
    print '%d episodes found' % (len(episodes))
    for e in episodes:
        e_data = get_url_data(e)
        if e_data == None:
            print 'Episode not found'
            continue
        audio_lk = scrape_audio(e_data)
        if audio_lk == None:
            print 'Audio link not found'
            continue
        random_sleep()
        audio_data = get_url_data(audio_lk)
        if audio_data == None:
            print 'Audio data not found'
            continue
        random_sleep(3, 7)
        filename = audio_lk.split('/')[-1]
        write_data(audio_data, filename)

if __name__ == '__main__':
    import sys
    assert len(sys.argv) == 2
    assert G_ROOT in sys.argv[1]
    download_series(sys.argv[1])

