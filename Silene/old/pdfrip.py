#!/usr/bin/python

# 
# Izaak 2014
#

import bs4, urllib2

def write_data(res, filename):
    if res == None or len(filename) == 0:
        return
    sane = "".join([x if x.isalnum() else "-" for x in filename])
    print 'Writing: %s' % sane 
    f = open(sane, 'wb')
    f.write(res)
    f.close()

def get_url_data(url):
    import time, random
    try:
       # l = urllib2.quote(url[7:])
       # url = 'http://' + l
       # print url
        print "Downloading: %s" % (url,)
        data = urllib2.urlopen(url)
        data_read = data.read()
        time.sleep(random.choice(xrange(1,20)))
        return data_read
    except urllib2.HTTPError as e:
        print 'HTTP error, ignoring ' + str(e.msg)
        return None
    except urllib2.URLError as e:
        print 'URLLib error, ignoring ' + str(e.msg)
        return None

def pdfrip(root, relurl):
    data = get_url_data(root + relurl) 
    if data == None: return
    soup = bs4.BeautifulSoup(data)
    got_pdfs = []
    lks = soup.find_all('a')
    for l in lks:
        href = l.get('href')
        if href == None:
            continue
        if 'pdf' in href.lower():
            l_f = ''
            if href.startswith('http://') or href.startswith('https://'):
                l_f = href
            else:
                if href.startswith('/'):
                    l_f = root + href
                else:
                    l_f = root + '/' + href
            if l_f not in got_pdfs:
                l_data = get_url_data(l_f)
                filename = href.split('/')[-1]
                write_data(l_data, filename)
                got_pdfs.append(l_f)
            else:
                print 'Already downloaded in this session: %s' % l_f

if __name__ == '__main__':
    import sys
    pdfrip(sys.argv[1], sys.argv[2])

