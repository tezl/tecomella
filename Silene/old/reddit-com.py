

def scrape(data):
    import bs4
    out = []
    soup = bs4.BeautifulSoup(data)
    es = soup.findAll('div', class_='entry')
    if es is None: return {'links':[]} 

    for e in es:
        article_url = u'' 
        title = e.find('a', class_='title')
        if title is not None:
            article_url = title.get('href')
            title = title.text
        comment_url = e.find('a', class_='comments')
        if comment_url is not None: comment_url = comment_url.get('href')

        out.append({'title':title,
                    'article_url':article_url,
                    'comment_url':comment_url}) 
    return {'links':out}
    

if __name__ == '__main__':
    import requests

    c = requests.get('http://www.reddit.com/r/coding')
    out = scrape(c.content)
    print(out)
