#!/bin/env python

#
# Izaak 2014
#

from bs4 import BeautifulSoup

import urllib2
TEXTFILES = 'archive.textfiles.com'
G_opener = urllib2.build_opener()
G_opener.addheaders = [('User-agent',\
    'Mozilla/5.0 (Windows NT x.y; Win64; x64; rv:10.0) Gecko/20100101 Firefox/10.0')]

def getlink(lk, sleepy=5):
    global G_opener
    print 'downloading %s' % (lk)
    u = G_opener.open('http://%s/%s' % (TEXTFILES, lk))
    f = open(lk, 'wb')
    f.write(u.read())
    f.close()
    import time
    time.sleep(sleepy)

map(getlink, [ lk.get('href') for lk in\
        BeautifulSoup(G_opener.open('http://%s' % (TEXTFILES))).\
            find_all('a') if lk.get('href').endswith('.tar.gz') ])

