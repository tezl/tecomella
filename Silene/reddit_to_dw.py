#!/usr/bin/env python

import sys
import json
import feedparser
import random
import requests
import requests.auth
import string
import codecs, locale
import os.path
sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout) 

def read_key(fname):
    if os.path.isfile(fname):
        try:
            f = open(fname)
            ls = f.read().split('\n')
            f.close()
            return {'username':ls[0],\
                    'password':ls[1],\
                    'key':ls[2],\
                    'token':ls[3]}
        except IOError:
            print('error reading configuration file')
    else:
        print('configuration file does not exist')
    return {}

USER_AGENT = "noks 0.1 by gityk"

def reddit_auth_token(creds):
    client_auth = requests.auth.HTTPBasicAuth(creds['key'], creds['token'])
    post_data = {"grant_type": "password", "username": creds['username'],
                "password": creds['password']}
    headers = {'User-Agent' : USER_AGENT}
    response = requests.post("https://www.reddit.com/api/v1/access_token",
                                auth=client_auth, data=post_data, headers=headers)
    out =response.json()
    return out

def req(tok, api_path):
    headers = {"Authorization": "bearer %s" %  (tok['access_token']),
               'User-Agent' : USER_AGENT }
    r = requests.get("https://oauth.reddit.com/%s" % (api_path), headers=headers)

    return r.json()


def usage():
    print('reddit_to_dw.py --config-file CONFIG_FILE subreddit new|hot|top|controversial')

if __name__ == '__main__':
    if len(sys.argv) != 5:
        usage()
        sys.exit(1)

    config_p, config_f, subreddit, list_type = sys.argv[1:5]
    if config_p not in ['--config-file', '-c']:
        usage()
        sys.exit(1)
    if list_type not in ['new', 'hot', 'top', 'controversial']:
        usage()
        sys.exit(1)
    
    key = read_key(config_f)
    if key == {}: sys.exit(1)

    tok = reddit_auth_token(key)
    its = (req(tok, '/r/%s/%s?limit=100&raw_json=1' %\
            (subreddit, list_type)))
    
    def title_to_filename(t):
        t = t.lower()
        randstr = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(4))
        return  u"".join([x if (x.isalnum()) else "-" for x in t][:64]) + '-' +  randstr

    lks = []
    for c in its['data']['children']:
        title = unicode(c['data']['title'])
        fname= title_to_filename(title)
        url = unicode(c['data']['url'])
        url_lower = url.lower()
        if ('youtube' in url_lower) or ('youtu.be' in url_lower) or ('vimeo' in url_lower):
            # TODO: possibly broken
            fname += '.mp4'
            fnd = True
        else:
            fnd = False
            for e in ['.pdf', '.png', '.gif', '.jpg']:
                if e in url_lower:
                    fname += e
                    fnd = True
                    break
            if (not fnd) and ('http://imgur.com' in url_lower) or ('https://imgur.com' in url_lower):
                img_name = url.split('/')[-1]
                url = 'http://i.imgur.com/%s.jpg' % (img_name)
                fname += '.jpg'
                fnd = True
            if (not fnd) and ('flickr.com' in url_lower) or ('flickr.com' in url_lower):
                continue # scraping flickr is pointless
            if not fnd:
                fname += '.pdf' # will be processed with phantomjs

        if '/comments' in url: continue # filter self posts
        lks.append({'url':url, 'filename':fname, 'title':title})

    print(json.dumps(lks, ensure_ascii=False))

