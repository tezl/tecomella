#!/bin/bash
shopt -s globstar;
shopt -s nullglob;
shopt -s nocaseglob;
for n in *; do
    if [ -d "$n" ]; then
    echo "changing to ${n}";
    cd ${n};
    for x in *.m4v; do
        echo "resizing ${x}";
        avconv -i  "${x}" -c:v mpeg4 -c:a libfdk_aac -s 320x240 rsz.mp4
        echo "removing old ${x}";
        rm "${x}";
    done;
    cd ../;
    fi;
done;

