#!/bin/bash
set -x
base_dir="";
data_dir="";
CARLINA_SERVER="";
CARLINA_PORT="";

if [ ! -d "$data_dir" ]; then
    echo 'data directory does not exit';
    exit;
fi;

out_dirs=();
today=$(date +"%F");
today_archive="archive-$today";
todays_dir="$base_dir/$today_archive";

if [ ! -d "$todays_dir" ]; then
    mkdir "$todays_dir";
fi;

function go {
    fname="$1";
    typ="$2";
    period="$3";
    
    while read line
        do
            if [ "$line" == "" ]; then  continue ; fi;
            first=$(echo "$line" | sed -r  -n 's/(^.+) (.+)/\1/p');
            second=$(echo "$line" | sed -r  -n 's/(^.+) (.+)/\2/p');
            out_dir="$todays_dir/$first";
            dw_sh_gen_cmd="dw_sh_gen -c guess -d $out_dir -s $CARLINA_SERVER -p $CARLINA_PORT -u $data_dir/user_agents.txt";
            if [ ! -d "$out_dir" ]; then
                mkdir "$out_dir";
            fi;
            out_dirs+=( $out_dir );
            if [ "$typ" == "rss" ]; then
                rss_to_dw --transform --period $period "$second" | $dw_sh_gen_cmd > "$out_dir/x.sh";
            elif [ "$typ" == "reddit" ]; then
                reddit_to_dw --config-file "$data_dir/reddit-auth.key" "$first" "$second" | $dw_sh_gen_cmd > "$out_dir/x.sh"; 
            elif [ "$typ" == "podcast" ]; then
                rss_to_dw --transform --enclosure --period $period "$second" | dw_sh_gen -c wget -d "$out_dir" -s "$CARLINA_SERVER" -p "$CARLINA_PORT" -u "$data_dir/user_agents.txt" > "$out_dir/x.sh";
            fi;
    
    done < "$fname";
}

month_day=$(date +"%d");
week_day=$(date +"%u");

go "$data_dir/rss_daily.txt" "rss" "daily";
go "$data_dir/subreddits_daily.txt" "reddit" "daily";
if [ "$month_day" -eq 1 ] ; then
    go "$data_dir/rss_monthly.txt" "rss" "monthly" ;
    go "$data_dir/subreddits_monthly.txt" "reddit" "monthly" ;
fi;
if [ "$week_day" -eq 6 ] ; then
    go "$data_dir/rss_weekly.txt" "rss" "weekly";
    go "$data_dir/subreddits_weekly.txt" "reddit" "weekly"; 
    go "$data_dir/podcasts_weekly.txt" "podcast" "weekly";

    vimeo_dir="$todays_dir/vimeo";
    mkdir "$vimeo_dir";

    vimeo_to_dw -d "$data_dir" -k "$data_dir/vimeo-auth.key" -c "$data_dir/vimeo-categories.txt" | dw_sh_gen -c youtube-dl -d "$vimeo_dir" -s "$CARLINA_SERVER" -p "$CARLINA_PORT" -u "$data_dir/user_agents.txt" > "$vimeo_dir/x.sh";

    chmod +x "$vimeo_dir/x.sh";
    "$vimeo_dir/x.sh";

fi;

for n in ${out_dirs[@]}; do
    chmod +x "$n/x.sh";
    "$n/x.sh";
    if [ $(ls "$n" | grep -E -c '*.(doc|docx|pdf)') -ne 0 ]; then
        mkdir "$n/doc";
        mv "$n/"*.{doc,docx,pdf} "$n/doc";
    fi;

    if [ $(ls "$n" | grep -E -c '*.(gif|jpg|png)') -ne 0 ]; then
        mkdir "$n/img";
        mv "$n/"*.{gif,jpg,png} "$n/img";
    fi;

    if [ $(ls "$n" | grep -E -c '*.(mp4|mkv)') -ne 0 ]; then
        mkdir "$n/vid";
        mv "$n/"*.{mp4,mkv} "$n/vid";
    fi;

    if [ $( echo "$n" | grep -E -c "kkkk" ) -eq 1 ]; then
        rm -r "$n"/{doc,vid};
        mv "$n" ${n%kkkk}pic;
    fi;
done;


find "$base_dir" -maxdepth 1 -mtime +5 -type d -exec rm -rv {} \;

