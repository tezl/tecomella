#!/usr/bin/env python

import feedparser
import os
import sys
import json
import random
import string
import sys
import codecs, locale
import time
import datetime

sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout) 

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--transform', help='transform feed ready for downloading', action='store_true')
    parser.add_argument('-p', '--period', help='daily|weekly|monthly', type=str, required=False)
    parser.add_argument('-e', '--enclosure', help='extract enclosed files from RSS', action='store_true')
    parser.add_argument('url', help='url for feed')
    args = parser.parse_args()

    period = args.period

    trans = args.transform
    url = unicode(args.url)
    encl = args.enclosure

    if period is not None and (period not in ['daily', 'weekly', 'monthly']):
        parser.error('period must be one of "daily", "weekly", "monthly"')
        sys.exit(1)

    if not ('http' in url or 'https' in url):
        parser.error('url must start with "http://" or "https://"')
        sys.exit(1)

    feed = feedparser.parse(url)

    def title_to_filename(t):
        t = t.lower()
        randstr = u''.join(random.choice(string.ascii_letters + string.digits)\
                        for _ in range(4))
        return u"".join([x if (x.isalnum())\
                    else u"-" for x in t][:64]) 

    def make_fname(title, url):
        fname = title_to_filename(title)
        url = url.lower()
        if '?' in url:
            url = url.split('?')[0]
        # TODO: FIXME: this is copypasta shared with
        # reddit_to_dw.py -- if it gets any longer, make
        # a new function and share it between the files
        if ('youtube' in url) or ('youtu.be' in url)\
            or ('vimeo' in url) or ('video' in title.lower()):
            fname += '.mp4'
        else:
            fnd = False
            for e in ['.pdf', '.png', '.gif', '.jpg', '.mp3']:
                if e in url:
                    fname += e
                    fnd = True
                    break
            if (not fnd) and ('http://imgur.com' in url) or ('https://imgur.com' in url):
                img_name = url.split('/')[-1]
                url = 'http://i.imgur.com/%s.jpg' % (img_name)
                fname += '.jpg'
                fnd = True
            if not fnd:
                fname += '.pdf' # will be processed with phantomjs
        return fname

    def todatetime(tstr):
        return datetime.datetime.fromtimestamp(time.mktime(tstr))

    def period_delta(period):
       return { None : None,\
                'daily':datetime.timedelta(days=1),\
                'weekly':datetime.timedelta(days=7),\
                'monthly':datetime.timedelta(days=30)}[period]

    lks = []
    now = datetime.datetime.now()
    pdelta = period_delta(period)
    for f in feed.entries:
        pub_time = todatetime(f['published_parsed'])
        if pdelta is not None:
            if (now - pub_time) > pdelta: continue 
        if trans:
            out = {}
            if encl:
                out = []
                ct = 1 
                def mk_record(m):
                    return {'url':m['url'],
                            'title':'%d-%s' % (ct, f['title']),
                            'filename':'%d-%s' % (ct, make_fname(f['title'], m['url']))} 
                if f.has_key('media_content'): # RSS 2.0
                    for mc in f['media_content']:
                        out.append(mk_record(mc))
                        ct += 1
                elif f.has_key('links') and len(f['links']) > 1: # Atom
                    for l in f['links']:
                        if l['rel'].lower() == 'enclosure':
                            out.append(mk_record(l))
            else:
                out = [{'url': f['link'],
                        'title':f['title'] ,
                        'filename':make_fname(f['title'], f['link'])}]
            lks.extend(out)

        else:
            lks.append( {'link':f['link'], 'title':f['title']} )
    print(json.dumps(lks, ensure_ascii=False))

