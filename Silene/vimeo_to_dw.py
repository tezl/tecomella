#!/usr/bin/env python

import bs4
import requests
import re
import os
from os.path import join, isdir
import pickle
import argparse
import json
import vimeo
import random
import string
import codecs, locale
import sys
sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout) 

def readfile(fname, error_msg=''):
    if os.path.isfile(fname):
        try:
            f = open(fname, 'r')
            ls = f.read().split('\n')
            f.close()
            return ls
        except IOError:
            print('error reading %s' % (fname,))
    else:
        print('file %s does not exist' % (fname,))
    return []


def read_key(fname):
    ls = readfile(fname)
    if ls != []:
        return {'key':ls[0],\
                'secret':ls[1],\
                'token':ls[2]}
    return {}

def make_client(auth):
    return vimeo.VimeoClient(key=auth['key'],
                       secret=auth['secret'],
                       token=auth['token'])

def read_cats(fname):
    cs = readfile(fname)
    return cs

class CatPos(object):
    def __init__(self, name='', page_number=1):
        self.name = name 
        self.page_number = page_number

def read_pos(data_dir):
    if os.path.isfile(data_dir + '/cat_pos.pickle'):
        f = open(data_dir + '/cat_pos.pickle', 'rb')
        p = pickle.load(f)
        return p
    return {}

def write_pos(cat_pos, data_dir):
    f = open(data_dir + '/cat_pos.pickle', 'wb')
    pickle.dump(cat_pos, f)

def title_to_filename(t):
    t = t.lower()
    randstr = u''.join(random.choice(string.ascii_letters + string.digits)\
                    for _ in range(4))
    return u"".join([x if (x.isalnum())\
                else u"-" for x in t][:64]) 

def go(vc, args):
    cs = readfile(args.category_file)
    cps = read_pos(args.data_directory)
    for c in cs:
        if not cps.has_key(c) and c != '':
            cps[c] = CatPos(c, 1)
    
    rejs = []
    out = []
    for k, v in cps.iteritems():
        if k not in cs:
            rejs.append(k)
            continue
        obj = vc.get('/categories/%s/videos?sort=relevant&direction=desc&page=%d&per_page=5'  %(v.name, v.page_number))
        if obj is None: continue
        data = obj.json().get('data')
        if data is None: continue
        for j in data: 
            out.append({'title':j['name'],
                        'url':j['link'],
                        'filename':title_to_filename(j['name']) + '.mp4' })
        cps[k].page_number += 1
    for r in rejs:
        del cps[r]

    write_pos(cps, args.data_directory)
    print(json.dumps(out, ensure_ascii=False))

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data-directory', help='search directory for data files', required=True)
    parser.add_argument('-k', '--key-file', help="configuration file", type=str, required=True)
    parser.add_argument('-c', '--category-file', help='list of categories', type=str, required=True)
    #parser.add_argument('-p', '--period', help='limit to period', type=str, required=False)
    args = parser.parse_args()

    k = read_key(args.key_file)
    if k != {}:
        vc = make_client(k)
        go(vc, args)


